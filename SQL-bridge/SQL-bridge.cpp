// SQL-bridge.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "SQL-bridge.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CSQLBridge

BEGIN_MESSAGE_MAP(CSQLBridge, CWinApp)
END_MESSAGE_MAP()


// CSQLBridge construction

CSQLBridge::CSQLBridge()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CSQLBridge object

CSQLBridge theApp;


// CSQLBridge initialization

BOOL CSQLBridge::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

extern "C" BOOL PASCAL EXPORT ExportedFunction()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	// normal function body here


	return TRUE;
}


//string readstr = new string(' ', 4000);

typedef void (*GetMessageLogFunction)(CString &log);
CString g_strLastMessage;

void SetLastMessage(LPCTSTR msg)
{
	g_strLastMessage = msg;
}

bool UpdateLastMessage(HMODULE hModule)
{
	// Get message buffer from current module
	GetMessageLogFunction logfunc = (GetMessageLogFunction)GetProcAddress((HMODULE)hModule, "GetMessageLog");
	if( logfunc )
	{
		/*CString csTmp;
		logfunc(csTmp);
		g_strLastMessage += csTmp;*/
		logfunc(g_strLastMessage);

		return true;
	}
	else
	{
		SetLastMessage(_T("Varning! Det gick inte att h�mta meddelanden fr�n modulen."));
		return false;
	}
}


void GetLogMessages(char* pszMessage)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	sprintf(pszMessage, "%S", g_strLastMessage);
}


#include <sqlapi.h>
//#include "C:\Program Files (x86)\SQLAPI\include\sqlapi.h"
//#include "C:\Program Files (x86)\SQLAPI 4.0.3\include\sqlapi.h"

int OpenConnection(DB_CONNECTION_DATA *pCon, char* szServer, char* szDatabase, char* szUser, char* szPassword)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	try
	{
		if( pCon->conn != 0)
		{
			if( pCon->conn->isConnected() == true )
			{
				return 0;
			}
		}
		else
		{
			pCon->admin_conn = NULL;
			pCon->conn = new SAConnection();
		}

		SAString sDBString = "";
		SAString sServer = "";
		SAString sDatabase = "";
		SAString sUsername = "";
		SAString sPassword = "";
		BOOL bWindowsAuth = false;

		/*sServer = "AGUS4\\SQLEXPRESS";
		sDatabase = "BD";
		bWindowsAuth = FALSE;
		sUsername = "sa";
		sPassword = "sa";*/

		sServer = szServer;
		sDatabase = szDatabase;

		char szBuf[2048];
		sprintf(szBuf, "%s@%s", szServer, szDatabase);
		sDBString = szBuf;
		//sDBString.Format(L"%s@%s", sServer, sDatabase);

		if(strcmp(szUser, "") != 0)
		{
			sUsername = szUser;
			sPassword = szPassword;
			bWindowsAuth = false;
		}
		else
		{
			bWindowsAuth = true;
		}


		pCon->conn->setClient( SA_SQLServer_Client );

		if( bWindowsAuth )
			pCon->conn->Connect(sDBString, "", "", SA_SQLServer_Client);
		else
			pCon->conn->Connect(sDBString, sUsername, sPassword); //, SA_SQLServer_Client);

		if( pCon->conn->isConnected() == false )
		{
			throw NULL;
		}


		//TODO: se till att tabeller �r uppdaterade
		doUpdateTables(szDatabase);
	}
	catch(SAException &x)
	{
		try
		{
			pCon->conn->Rollback();
		}
		catch(SAException &)
		{
		}

		return -1;
	}

	return 0;
}

int CloseConnection(DB_CONNECTION_DATA *pCon)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	try
	{
		if(pCon->conn != 0)
		{
			if( pCon->conn->isConnected() && pCon->conn->isAlive() )
			{
				pCon->conn->Disconnect();
				pCon->conn->Destroy();
				pCon->conn = 0;
			}
		}
	}
	catch(SAException &x)
	{
		return -1;
	}

	return 0;
}


typedef BOOL (*ImportFileFunction)(CString file, LPCTSTR templateXml, LPCTSTR username, DB_CONNECTION_DATA &conn, int* standId);
typedef BOOL (*CalculateStandFunction)(int standId, DB_CONNECTION_DATA &conn);
//typedef void (*GetLogMessages)(LPCTSTR pszMessage);

BOOL ImportFile2(char* file, char* templateXml, char* username, DB_CONNECTION_DATA &conn, int *traktId)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	BOOL bReturn = FALSE;
	CString csFile = CString(file);
	CString csTemplateXml = CString(templateXml);
	CString csUsername = CString(username);

	// load UMIndata.dll
	HINSTANCE hModule = LoadLibrary(_T("Modules\\UMIndata.dll"));	//getDllDirectory() + _T("\\Modules\\UMEstimate.dll"));
	if( hModule )
	{
		SetLocaleInfo(GetSystemDefaultLCID(), LOCALE_SDECIMAL, _T("."));
		SetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, _T("."));

		ImportFileFunction func = (ImportFileFunction)GetProcAddress((HMODULE)hModule, "ImportFile");
		bReturn = func(csFile, csTemplateXml, csUsername, conn, traktId);

		// get last messages from module
		UpdateLastMessage(hModule);

		FreeLibrary(hModule);
	}

	return bReturn;
}

BOOL calculateStand2(int trakt_id, int* pConn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	BOOL bReturn = FALSE;
	DB_CONNECTION_DATA db;
	memcpy(&db, pConn, sizeof(DB_CONNECTION_DATA));

	// load UMEstimate.dll
	HINSTANCE hModule = LoadLibrary(_T("Modules\\UMEstimate.dll"));	//getDllDirectory() + _T("\\Modules\\UMEstimate.dll"));
	if( hModule )
	{
		SetLocaleInfo(GetSystemDefaultLCID(), LOCALE_SDECIMAL, _T("."));
		SetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, _T("."));

		CalculateStandFunction func = (CalculateStandFunction)GetProcAddress((HMODULE)hModule, "calculateStand");
		bReturn = func(trakt_id, db);

		// get last messages from module
		UpdateLastMessage(hModule);

		FreeLibrary(hModule);
	}

	return bReturn;
}


typedef BOOL (*DoAlterTables)(LPCTSTR db_name);
BOOL doUpdateTables(char *db_name)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString csDbName = CString(db_name);
	BOOL bReturn = FALSE;

	// load UMEstimate.dll
	HINSTANCE hModule = LoadLibrary(_T("Modules\\UMEstimate.dll"));	//getDllDirectory() + _T("\\Modules\\UMEstimate.dll"));
	if( hModule )
	{
		SetLocaleInfo(GetSystemDefaultLCID(), LOCALE_SDECIMAL, _T("."));
		SetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, _T("."));

		DoAlterTables func = (DoAlterTables)GetProcAddress((HMODULE)hModule, "DoAlterTables");
		bReturn = func(csDbName);

		// get last messages from module
		UpdateLastMessage(hModule);

		FreeLibrary(hModule);
	}

	return bReturn;
}
