// SQL-bridge.h : main header file for the SQL-bridge DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CSQLBridge
// See SQL-bridge.cpp for the implementation of this class
//

class CSQLBridge : public CWinApp
{
public:
	CSQLBridge();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

#include <sqlapi.h>
typedef struct _db_connection_data
{
	SAConnection* conn;
	SAConnection* admin_conn;
} DB_CONNECTION_DATA;


extern "C" __declspec(dllexport) int OpenConnection(DB_CONNECTION_DATA*, char* szServer, char* szDatabase, char* szUser, char* szPassword);
extern "C" __declspec(dllexport) int CloseConnection(DB_CONNECTION_DATA*);
extern "C" __declspec(dllexport) int ImportFile2(char* file, char* templateXml, char* username, DB_CONNECTION_DATA &conn, int *traktId);
extern "C" __declspec(dllexport) int calculateStand2(int trakt_id, int* pConn);
extern "C" __declspec(dllexport) void GetLogMessages(char* pszMessage);
extern "C" __declspec(dllexport) int doUpdateTables(char *db_name);
