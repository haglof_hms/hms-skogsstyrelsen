﻿function ConfirmRemove(e) {
    if (window.confirm("Är du säker att du vill radera?")) {
        e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
    }
    return false;
}
