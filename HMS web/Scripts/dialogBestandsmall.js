$(function () {
    $('#dialogBestandsmall').dialog({
        autoOpen: false,
        dialogClass: 'dialogBestandsmall',
        width: 500,
        height: 560,
        modal: true,
        title: "Information om beståndsmall",
        buttons: {
            'Stäng': function () {
                $(this).dialog('close');
            }
        },
        open: function (event, ui) {
            /*$('#dialogBestandsmall').html('').find('.tabs').tabs();*/
        }
    });

    $('#visaBestandsmall').click(function () {
        var createFormUrl = $(this).attr('href');
        $('#dialogBestandsmall').html('')
        .load(createFormUrl, function () {
            /* The createGenreForm is loaded on the fly using jQuery load. 
             In order to have client validation working it is necessary to tell the 
             jQuery.validator to parse the newly added content */
            jQuery.validator.unobtrusive.parse('#createBestandsmallForm');
            $('#dialogBestandsmall').dialog('open');
        });

        return false;
    });

});
