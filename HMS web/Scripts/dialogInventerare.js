﻿$(function () {
    $('#dialogVisaInventerare').dialog({
        autoOpen: false,
        dialogClass: 'dialogVisaInventerare',
        width: 600,
        height: 500,
        modal: true,
        title: "Inventerare",
        buttons: {
            'Stäng': function () {
                $(this).dialog('close');
            }
        }
    });

    $('#inventerareShowLink').click(function () {
        var createFormUrl = $(this).attr('href');
        $('#dialogVisaInventerare').html('')
        .load(createFormUrl, function () {
            $('#dialogVisaInventerare').dialog('open');
        });

        return false;
    });

    $('#dialogSkapaInventerare').dialog({
        autoOpen: false,
        dialogClass: 'ChooseInventerareCSS',
        width: 350,
        height: 300,
        modal: true,
        title: 'Lägg till inventerare',
        buttons: {
            'Spara': function () {
                var createInventerareForm = $('#createInventerareForm');
                if (createInventerareForm.valid()) {
                    $.post(createInventerareForm.attr('action'), createInventerareForm.serialize(), function (data) {
                        if (data.inventerare_id == -1) {
                            alert(data.Error);
                        }
                        else {
                            /* check that we have not choosen this already */
                            var listBox = document.getElementById('InventerareList');
                            for (var x = 0; x < listBox.options.length; x++) {
                                if (listBox.options[x].value == data.inventerare_id) {
                                    return;
                                }
                            }

                            /* Add the new Inventerare to the dropdown list and select it */
                            $('#InventerareList').append(
                                    $('<option></option>')
                                        .val(data.inventerare_id)
                                        .html(data.inventerare_efternamn + ' ' + data.inventerare_namn)
                                        .prop('selected', true)  /* Selects the new Inventerare in the DropDown LB */
                                );
                            $('#dialogSkapaInventerare').dialog('close');
                        }
                    });
                }
            },
            'Avbryt': function () {
                $(this).dialog('close');
            }
        }
    });

    $('#inventerareAddLink').click(function () {
        var createFormUrl = $(this).attr('href');
        $('#dialogSkapaInventerare').html('')
        .load(createFormUrl, function () {  
            /* The createGenreForm is loaded on the fly using jQuery load. 
             In order to have client validation working it is necessary to tell the 
             jQuery.validator to parse the newly added content */
            jQuery.validator.unobtrusive.parse('#createInventerareForm');
            $('#dialogSkapaInventerare').dialog('open');
        });

        return false;
    });

});
