﻿$(function () {
    $('#dialogVisaFastighet').dialog({
        autoOpen: false,
        dialogClass: 'dialogVisaFastighet',
        width: 800,
        height: 500,
        modal: true,
        title: "Fastigheter",
        buttons: {
        'Stäng': function () {
            $(this).dialog('close');
        }
    }
});

    $('#visaFastigheter').click(function () {
        var createFormUrl = $(this).attr('href');
        $('#dialogVisaFastighet').html('')
        .load(createFormUrl, function () {
            jQuery.validator.unobtrusive.parse('#createFastighetForm');
            $('#dialogVisaFastighet').dialog('open');
        });

        return false;
    });

    $('#dialogSkapaFastighet').dialog({
        autoOpen: false,
        dialogClass: 'dialogSkapaFastighet',
        width: 500,
        height: 600,
        modal: true,
        title: "Fastigheter",
        buttons: {
            'Spara': function () {
                var createFastighetForm = $('#createFastighetForm');
                if (createFastighetForm.valid()) {
                    $.post(createFastighetForm.attr('action'), createFastighetForm.serialize(), function (data) {
                        if (data.id == -1) {
                            alert("Gick inte att skapa fastighet!");
                        }
                        else {
                            $('#textFastighetsnamn').val(data.prop_full_name);
                            $('#hiddenFastighetsId').val(data.id);
                            $('#dialogSkapaFastighet').dialog('close');
                        }
                    });
                }
            },
            'Avbryt': function () {
                $(this).dialog('close');
            }
        }
    });

    $('#skapaFastighet').click(function () {
        var createFormUrl = $(this).attr('href');
        $('#dialogSkapaFastighet').html('')
        .load(createFormUrl, function () {
            jQuery.validator.unobtrusive.parse('#createFastighetForm');
            $('#dialogSkapaFastighet').dialog('open');
        });

        return false;
    });

});
