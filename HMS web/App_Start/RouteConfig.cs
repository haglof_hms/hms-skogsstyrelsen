﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HMS_web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*routes.MapRoute(
                name: "SearchTrakt",
                url: "Search/Page/{page}",
                defaults: new { controller = "Search", action = "Index"}
            );*/

            routes.MapRoute(
                "DklsRoute", // Route name
                "Bestand/AddDiameterklassItem/{trakt}/{yta}/{dkls}",
                defaults: new { controller = "Bestand", action = "AddDiameterklassItem", trakt = UrlParameter.Optional, yta = UrlParameter.Optional, dkls = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}