﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HMS_web.Models
{
    [MetadataType(typeof(esti_trakt_dcls_trees_table_metadata))]
    public partial class esti_trakt_dcls_trees_table
    {
    }

    public class esti_trakt_dcls_trees_table_metadata
    {
        /*public esti_trakt_dcls_trees_table()
        {
            this.esti_trakt_trees_assort_table = new HashSet<esti_trakt_trees_assort_table>();
        }*/

        public object tdcls_id { get; set; }
        public object tdcls_trakt_id { get; set; }
        public object tdcls_plot_id { get; set; }
        public object tdcls_spc_id { get; set; }
        //[Required(ErrorMessage = "Beståndsnamn måste fyllas i!")]
        [Display(Name = "Trädslag")]
        public object tdcls_spc_name { get; set; }
        [Display(Name = "Klass-")]
        public object tdcls_dcls_from { get; set; }
        [Display(Name = "-Klass")]
        public object tdcls_dcls_to { get; set; }
        [Display(Name = "Höjd")]
        public object tdcls_hgt { get; set; }
        [Display(Name = "Antal")]
        public object tdcls_numof { get; set; }
        [Display(Name = "Grönkronegräns")]
        public object tdcls_gcrown { get; set; }
        [Display(Name = "Barktjocklek")]
        public object tdcls_bark_thick { get; set; }
        [Display(Name = "Volym m3sk")]
        public object tdcls_m3sk { get; set; }
        [Display(Name = "Volym m3fub")]
        public object tdcls_m3fub { get; set; }
        [Display(Name = "Volym m3ub")]
        public object tdcls_m3ub { get; set; }
        [Display(Name = "Grot")]
        public object tdcls_grot { get; set; }
        [Display(Name = "Ålder")]
        public object tdcls_age { get; set; }
        [Display(Name = "Tillväxt")]
        public object tdcls_growth { get; set; }
        [Display(Name = "Skapad")]
        public object created { get; set; }
        [Display(Name = "Antal kantträd")]
        public object tdcls_numof_randtrees { get; set; }
    
        //public virtual ICollection<esti_trakt_trees_assort_table> esti_trakt_trees_assort_table { get; set; }
        //public virtual esti_trakt_table esti_trakt_table { get; set; }
    }
}