//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class fst_contacts_table
    {
        public fst_contacts_table()
        {
            this.fst_categories_for_contacts_table = new HashSet<fst_categories_for_contacts_table>();
            this.fst_prop_owner_table = new HashSet<fst_prop_owner_table>();
        }
    
        public int id { get; set; }
        public string pnr_orgnr { get; set; }
        public string name_of { get; set; }
        public string company { get; set; }
        public string address { get; set; }
        public string post_num { get; set; }
        public string post_address { get; set; }
        public string county { get; set; }
        public string country { get; set; }
        public string phone_work { get; set; }
        public string phone_home { get; set; }
        public string fax_number { get; set; }
        public string mobile { get; set; }
        public string e_mail { get; set; }
        public string web_site { get; set; }
        public string notes { get; set; }
        public string created_by { get; set; }
        public System.DateTime created { get; set; }
        public string vat_number { get; set; }
        public Nullable<int> connect_id { get; set; }
        public Nullable<int> type_of { get; set; }
        public byte[] picture { get; set; }
        public string co_address { get; set; }
        public string bankgiro { get; set; }
        public string plusgiro { get; set; }
        public string bankkonto { get; set; }
        public string levnummer { get; set; }
        public string avinummer { get; set; }
    
        public virtual ICollection<fst_categories_for_contacts_table> fst_categories_for_contacts_table { get; set; }
        public virtual ICollection<fst_prop_owner_table> fst_prop_owner_table { get; set; }
    }
}
