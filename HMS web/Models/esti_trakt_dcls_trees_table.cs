//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class esti_trakt_dcls_trees_table
    {
        public esti_trakt_dcls_trees_table()
        {
            this.esti_trakt_trees_assort_table = new HashSet<esti_trakt_trees_assort_table>();
        }
    
        public int tdcls_id { get; set; }
        public int tdcls_trakt_id { get; set; }
        public Nullable<int> tdcls_plot_id { get; set; }
        public Nullable<int> tdcls_spc_id { get; set; }
        public string tdcls_spc_name { get; set; }
        public Nullable<double> tdcls_dcls_from { get; set; }
        public Nullable<double> tdcls_dcls_to { get; set; }
        public Nullable<double> tdcls_hgt { get; set; }
        public Nullable<double> tdcls_numof { get; set; }
        public Nullable<double> tdcls_gcrown { get; set; }
        public Nullable<double> tdcls_bark_thick { get; set; }
        public Nullable<double> tdcls_m3sk { get; set; }
        public Nullable<double> tdcls_m3fub { get; set; }
        public Nullable<double> tdcls_m3ub { get; set; }
        public Nullable<double> tdcls_grot { get; set; }
        public Nullable<int> tdcls_age { get; set; }
        public Nullable<int> tdcls_growth { get; set; }
        public System.DateTime created { get; set; }
        public Nullable<int> tdcls_numof_randtrees { get; set; }
    
        public virtual ICollection<esti_trakt_trees_assort_table> esti_trakt_trees_assort_table { get; set; }
        public virtual esti_trakt_table esti_trakt_table { get; set; }
    }
}
