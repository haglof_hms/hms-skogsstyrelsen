﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HMS_web.Models
{
    [MetadataType(typeof(esti_trakt_sample_trees_table_metadata))]
    public partial class esti_trakt_sample_trees_table
    {
    }

    public class esti_trakt_sample_trees_table_metadata
    {
        public object ttree_id { get; set; }
        public object ttree_trakt_id { get; set; }
        public object ttree_plot_id { get; set; }
        public object ttree_spc_id { get; set; }
        //[Required(ErrorMessage = "Beståndsnamn måste fyllas i!")]
        [Display(Name = "Trädslag")]
        public string ttree_spc_name { get; set; }
        [Display(Name = "DBH")]
        public object ttree_dbh { get; set; }
        [Display(Name = "Höjd")]
        public object ttree_hgt { get; set; }
        [Display(Name = "Grönkronegräns")]
        public object ttree_gcrown { get; set; }
        [Display(Name = "Bark")]
        public object ttree_bark_thick { get; set; }
        [Display(Name = "Grot")]
        public object ttree_grot { get; set; }
        [Display(Name = "Ålder")]
        public object ttree_age { get; set; }
        [Display(Name = "Tillväxt")]
        public object ttree_growth { get; set; }
        public object ttree_tree_type { get; set; }
        public object ttree_dcls_from { get; set; }
        public object ttree_dcls_to { get; set; }
        [Display(Name = "Volym m3sk")]
        public object ttree_m3sk { get; set; }
        [Display(Name = "Volym m3fub")]
        public object ttree_m3fub { get; set; }
        [Display(Name = "Volym m3ub")]
        public object ttree_m3ub { get; set; }
        public object ttree_bonitet { get; set; }
        [Display(Name = "Skapad")]
        public object created { get; set; }
        [Display(Name = "Koordinat")]
        public object ttree_coord { get; set; }

        //public virtual esti_trakt_table esti_trakt_table { get; set; }
    }
}