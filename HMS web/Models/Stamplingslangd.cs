﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_web.Models
{
    public class Tradslag
    {
        public int? nId { get; set;  }
        public string sNamn { get; set; }
    }

    public class DiameterKlass
    {
        public DiameterKlass()
        {
            Tradslagslista = new List<Tradslag>();
        }

        public List<Tradslag> Tradslagslista { get; set; }

        [Display(Name = "Diameterklass")]
        [Range(1, int.MaxValue, ErrorMessage = "Värde måste vara större än 0!")]
        [Required(ErrorMessage = "Diameter måste fyllas i!")]
        public int? Klass { get; set; }
        [Display(Name = "Trädslag")]
        [Required(ErrorMessage = "Trädslag måste väljas!")]
        public int? TradslagId { get; set; }
        public string Tradslag { get; set; }
        [Display(Name = "Yta")]
        public int? Yta { get; set; }
        [Display(Name = "Antal träd")]
        [Required(ErrorMessage = "Antal träd måste fyllas i!")]
        public int? Antal { get; set; }
    }

    public class Provtrad
    {
        public Provtrad()
        {
            Tradslagslista = new List<Tradslag>();
            Yta = 0;
            Diameter = 0;
            Hojd = 0;
            TradslagId = 0;
            Tradslag = "";
            Alder = 0;
            Barktjocklek = 0;
        }

        public List<Tradslag> Tradslagslista { get; set; }

        [Display(Name = "Yta")]
        public int? Yta { get; set; }


        [Display(Name = "BHD (mm)")]
        [Range(60, int.MaxValue, ErrorMessage = "Minsta diameter är 60mm!")]
        [Required(ErrorMessage = "Diameter måste fyllas i!")]
        public int Diameter { get; set; }

        [Display(Name = "Höjd (dm)")]
        [Range(1, int.MaxValue, ErrorMessage = "Värde måste vara större än 0!")]
        [Required(ErrorMessage = "Höjd måste fyllas i!")]
        public int Hojd { get; set; }

        [Display(Name = "Grönkronegräns (dm)")]
//        [Range(1, int.MaxValue, ErrorMessage = "Värde måste vara större än 0!")]
//        [Required(ErrorMessage = "Grönkronegräns måste fyllas i!")]
        public int Gronkronegrans { get; set; }

        [Display(Name = "Trädslag")]
        [Required(ErrorMessage = "Trädslag måste väljas!")]
        public int? TradslagId { get; set; }
        public string Tradslag { get; set; }

        [Display(Name = "Ålder")]
//        [Range(1, int.MaxValue, ErrorMessage = "Värde måste vara större än 0!")]
//        [Required(ErrorMessage = "Ålder måste fyllas i!")]
        public int Alder { get; set; }

        [Display(Name = "Barktjocklek (mm)")]
//        [Range(1, int.MaxValue, ErrorMessage = "Värde måste vara större än 0!")]
//        [Required(ErrorMessage = "Barktjocklek måste fyllas i!")]
        public int Barktjocklek { get; set; }
    }

    public class Stamplingslangd
    {
        public Stamplingslangd()
        {
            DiameterKlasser = new List<DiameterKlass>();
            Provtrad = new List<Provtrad>();
        }

        public int nTraktId { get; set; }
        public int DiameterIntervall { get; set; }
        public List<DiameterKlass> DiameterKlasser { get; set; }
        public List<Provtrad> Provtrad { get; set; }
    }
}
