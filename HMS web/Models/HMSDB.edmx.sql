
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 05/17/2013 15:53:51
-- Generated from EDMX file: C:\Sources\w32\vC\HMS\HMS Skogsstyrelsen\HMS web\Models\HMSDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HMS_web3];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Category]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[fst_categories_for_contacts_table] DROP CONSTRAINT [FK_Category];
GO
IF OBJECT_ID(N'[dbo].[FK_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[fst_categories_for_contacts_table] DROP CONSTRAINT [FK_Contact];
GO
IF OBJECT_ID(N'[dbo].[FK_Contact1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[fst_prop_owner_table] DROP CONSTRAINT [FK_Contact1];
GO
IF OBJECT_ID(N'[dbo].[FK_esti_trakt_tableweb_TraktInventerare]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[web_TraktInventerareSet] DROP CONSTRAINT [FK_esti_trakt_tableweb_TraktInventerare];
GO
IF OBJECT_ID(N'[dbo].[fk_otcrot]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_rotpost_other_table] DROP CONSTRAINT [fk_otcrot];
GO
IF OBJECT_ID(N'[dbo].[FK_Property]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[fst_prop_owner_table] DROP CONSTRAINT [FK_Property];
GO
IF OBJECT_ID(N'[dbo].[fk_rot]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_rotpost_table] DROP CONSTRAINT [fk_rot];
GO
IF OBJECT_ID(N'[dbo].[fk_spcrot]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_rotpost_spc_table] DROP CONSTRAINT [fk_spcrot];
GO
IF OBJECT_ID(N'[dbo].[fk_tass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_spc_assort_table] DROP CONSTRAINT [fk_tass];
GO
IF OBJECT_ID(N'[dbo].[fk_tassort]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_trees_assort_table] DROP CONSTRAINT [fk_tassort];
GO
IF OBJECT_ID(N'[dbo].[fk_tdata]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_data_table] DROP CONSTRAINT [fk_tdata];
GO
IF OBJECT_ID(N'[dbo].[fk_tdcls]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_dcls_trees_table] DROP CONSTRAINT [fk_tdcls];
GO
IF OBJECT_ID(N'[dbo].[fk_tdcls1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_dcls1_trees_table] DROP CONSTRAINT [fk_tdcls1];
GO
IF OBJECT_ID(N'[dbo].[fk_tdcls2]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_dcls2_trees_table] DROP CONSTRAINT [fk_tdcls2];
GO
IF OBJECT_ID(N'[dbo].[fk_tplot]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_plot_table] DROP CONSTRAINT [fk_tplot];
GO
IF OBJECT_ID(N'[dbo].[fk_tprl]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_misc_data_table] DROP CONSTRAINT [fk_tprl];
GO
IF OBJECT_ID(N'[dbo].[fk_tsetspc1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_set_spc_table] DROP CONSTRAINT [fk_tsetspc1];
GO
IF OBJECT_ID(N'[dbo].[fk_ttrans]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_trans_assort_table] DROP CONSTRAINT [fk_ttrans];
GO
IF OBJECT_ID(N'[dbo].[fk_ttree]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[esti_trakt_sample_trees_table] DROP CONSTRAINT [fk_ttree];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[cost_template_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[cost_template_table];
GO
IF OBJECT_ID(N'[HMS_webModelStoreContainer].[esti_cutcls_table]', 'U') IS NOT NULL
    DROP TABLE [HMS_webModelStoreContainer].[esti_cutcls_table];
GO
IF OBJECT_ID(N'[HMS_webModelStoreContainer].[esti_hgtcls_table]', 'U') IS NOT NULL
    DROP TABLE [HMS_webModelStoreContainer].[esti_hgtcls_table];
GO
IF OBJECT_ID(N'[HMS_webModelStoreContainer].[esti_inpdata_table]', 'U') IS NOT NULL
    DROP TABLE [HMS_webModelStoreContainer].[esti_inpdata_table];
GO
IF OBJECT_ID(N'[HMS_webModelStoreContainer].[esti_origin_table]', 'U') IS NOT NULL
    DROP TABLE [HMS_webModelStoreContainer].[esti_origin_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_data_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_data_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_dcls_trees_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_dcls_trees_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_dcls1_trees_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_dcls1_trees_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_dcls2_trees_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_dcls2_trees_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_misc_data_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_misc_data_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_plot_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_plot_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_rotpost_other_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_rotpost_other_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_rotpost_spc_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_rotpost_spc_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_rotpost_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_rotpost_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_sample_trees_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_sample_trees_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_set_spc_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_set_spc_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_spc_assort_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_spc_assort_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_trans_assort_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_trans_assort_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_trees_assort_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_trees_assort_table];
GO
IF OBJECT_ID(N'[dbo].[esti_trakt_type_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[esti_trakt_type_table];
GO
IF OBJECT_ID(N'[dbo].[fst_categories_for_contacts_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_categories_for_contacts_table];
GO
IF OBJECT_ID(N'[dbo].[fst_category_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_category_table];
GO
IF OBJECT_ID(N'[dbo].[fst_contacts_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_contacts_table];
GO
IF OBJECT_ID(N'[dbo].[fst_county_municipal_parish_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_county_municipal_parish_table];
GO
IF OBJECT_ID(N'[dbo].[fst_external_doc_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_external_doc_table];
GO
IF OBJECT_ID(N'[dbo].[fst_postnumber_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_postnumber_table];
GO
IF OBJECT_ID(N'[dbo].[fst_prop_owner_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_prop_owner_table];
GO
IF OBJECT_ID(N'[dbo].[fst_property_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_property_table];
GO
IF OBJECT_ID(N'[dbo].[fst_species_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fst_species_table];
GO
IF OBJECT_ID(N'[dbo].[prn_pricelist_table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[prn_pricelist_table];
GO
IF OBJECT_ID(N'[HMS_webModelStoreContainer].[tmpl_template_table]', 'U') IS NOT NULL
    DROP TABLE [HMS_webModelStoreContainer].[tmpl_template_table];
GO
IF OBJECT_ID(N'[dbo].[web_inventerare]', 'U') IS NOT NULL
    DROP TABLE [dbo].[web_inventerare];
GO
IF OBJECT_ID(N'[dbo].[web_TraktInventerareSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[web_TraktInventerareSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'esti_cutcls_table'
CREATE TABLE [dbo].[esti_cutcls_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [cutcls_name] nvarchar(30)  NOT NULL,
    [notes] nvarchar(255)  NULL,
    [create_date] datetime  NOT NULL
);
GO

-- Creating table 'esti_hgtcls_table'
CREATE TABLE [dbo].[esti_hgtcls_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [hgtcls_name] nvarchar(30)  NOT NULL,
    [notes] nvarchar(255)  NULL,
    [create_date] datetime  NOT NULL
);
GO

-- Creating table 'esti_inpdata_table'
CREATE TABLE [dbo].[esti_inpdata_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [inpdata_name] nvarchar(30)  NOT NULL,
    [notes] nvarchar(255)  NULL,
    [create_date] datetime  NOT NULL
);
GO

-- Creating table 'esti_origin_table'
CREATE TABLE [dbo].[esti_origin_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [origin_name] nvarchar(30)  NOT NULL,
    [notes] nvarchar(255)  NULL,
    [create_date] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_data_table'
CREATE TABLE [dbo].[esti_trakt_data_table] (
    [tdata_id] int  NOT NULL,
    [tdata_trakt_id] int  NOT NULL,
    [tdata_data_type] smallint  NOT NULL,
    [tdata_spc_id] int  NULL,
    [tdata_spc_name] nvarchar(51)  NULL,
    [tdata_percent] float  NULL,
    [tdata_numof] int  NULL,
    [tdata_da] float  NULL,
    [tdata_dg] float  NULL,
    [tdata_dgv] float  NULL,
    [tdata_hgv] float  NULL,
    [tdata_gy] float  NULL,
    [tdata_avg_hgt] float  NULL,
    [tdata_h25] float  NULL,
    [tdata_m3sk_vol] float  NULL,
    [tdata_m3fub_vol] float  NULL,
    [tdata_m3ub_vol] float  NULL,
    [tdata_avg_m3sk_vol] float  NULL,
    [tdata_avg_m3fub_vol] float  NULL,
    [tdata_avg_m3ub_vol] float  NULL,
    [created] datetime  NOT NULL,
    [tdata_gcrown] float  NULL,
    [tdata_grot] float  NULL
);
GO

-- Creating table 'esti_trakt_dcls_trees_table'
CREATE TABLE [dbo].[esti_trakt_dcls_trees_table] (
    [tdcls_id] int  NOT NULL,
    [tdcls_trakt_id] int  NOT NULL,
    [tdcls_plot_id] int  NULL,
    [tdcls_spc_id] int  NULL,
    [tdcls_spc_name] nvarchar(20)  NULL,
    [tdcls_dcls_from] float  NULL,
    [tdcls_dcls_to] float  NULL,
    [tdcls_hgt] float  NULL,
    [tdcls_numof] float  NULL,
    [tdcls_gcrown] float  NULL,
    [tdcls_bark_thick] float  NULL,
    [tdcls_m3sk] float  NULL,
    [tdcls_m3fub] float  NULL,
    [tdcls_m3ub] float  NULL,
    [tdcls_grot] float  NULL,
    [tdcls_age] int  NULL,
    [tdcls_growth] int  NULL,
    [created] datetime  NOT NULL,
    [tdcls_numof_randtrees] int  NULL
);
GO

-- Creating table 'esti_trakt_dcls1_trees_table'
CREATE TABLE [dbo].[esti_trakt_dcls1_trees_table] (
    [tdcls1_id] int  NOT NULL,
    [tdcls1_trakt_id] int  NOT NULL,
    [tdcls1_plot_id] int  NULL,
    [tdcls1_spc_id] int  NULL,
    [tdcls1_spc_name] nvarchar(20)  NULL,
    [tdcls1_dcls_from] float  NULL,
    [tdcls1_dcls_to] float  NULL,
    [tdcls1_hgt] float  NULL,
    [tdcls1_numof] float  NULL,
    [tdcls1_gcrown] float  NULL,
    [tdcls1_bark_thick] float  NULL,
    [tdcls1_m3sk] float  NULL,
    [tdcls1_m3fub] float  NULL,
    [tdcls1_m3ub] float  NULL,
    [tdcls1_grot] float  NULL,
    [tdcls1_age] int  NULL,
    [tdcls1_growth] int  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_dcls2_trees_table'
CREATE TABLE [dbo].[esti_trakt_dcls2_trees_table] (
    [tdcls2_id] int  NOT NULL,
    [tdcls2_trakt_id] int  NOT NULL,
    [tdcls2_plot_id] int  NULL,
    [tdcls2_spc_id] int  NULL,
    [tdcls2_spc_name] nvarchar(20)  NULL,
    [tdcls2_dcls_from] float  NULL,
    [tdcls2_dcls_to] float  NULL,
    [tdcls2_hgt] float  NULL,
    [tdcls2_numof] float  NULL,
    [tdcls2_gcrown] float  NULL,
    [tdcls2_bark_thick] float  NULL,
    [tdcls2_m3sk] float  NULL,
    [tdcls2_m3fub] float  NULL,
    [tdcls2_m3ub] float  NULL,
    [tdcls2_grot] float  NULL,
    [tdcls2_age] int  NULL,
    [tdcls2_growth] int  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_misc_data_table'
CREATE TABLE [dbo].[esti_trakt_misc_data_table] (
    [tprl_trakt_id] int  NOT NULL,
    [tprl_name] nvarchar(50)  NULL,
    [tprl_typeof] int  NULL,
    [tprl_pricelist] nvarchar(max)  NULL,
    [tprl_costtmpl_name] nvarchar(50)  NULL,
    [tprl_costtmpl_typeof] int  NULL,
    [tprl_costtmpl] nvarchar(max)  NULL,
    [tprl_dcls] float  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_plot_table'
CREATE TABLE [dbo].[esti_trakt_plot_table] (
    [tplot_id] int  NOT NULL,
    [tplot_trakt_id] int  NOT NULL,
    [tplot_name] nvarchar(40)  NULL,
    [tplot_plot_type] int  NULL,
    [tplot_radius] float  NULL,
    [tplot_length1] float  NULL,
    [tplot_width1] float  NULL,
    [tplot_area] float  NULL,
    [tplot_coord] nvarchar(80)  NULL,
    [tplot_numof_trees] int  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_rotpost_other_table'
CREATE TABLE [dbo].[esti_trakt_rotpost_other_table] (
    [otcrot_id] int  NOT NULL,
    [otcrot_trakt_id] int  NOT NULL,
    [otcrot_value] float  NULL,
    [otcrot_sum_value] float  NULL,
    [otcrot_text] nvarchar(255)  NULL,
    [otcrot_percent] float  NULL,
    [otcrot_type] smallint  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_rotpost_spc_table'
CREATE TABLE [dbo].[esti_trakt_rotpost_spc_table] (
    [spcrot_id] int  NOT NULL,
    [spcrot_trakt_id] int  NOT NULL,
    [spcrot_post_spcname] nvarchar(20)  NULL,
    [spcrot_post_cost1] float  NULL,
    [spcrot_post_cost2] float  NULL,
    [spcrot_post_cost3] float  NULL,
    [spcrot_post_cost4] float  NULL,
    [spcrot_post_cost5] float  NULL,
    [spcrot_post_cost6] float  NULL,
    [spcrot_post_origin] smallint  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_rotpost_table'
CREATE TABLE [dbo].[esti_trakt_rotpost_table] (
    [rot_trakt_id] int  NOT NULL,
    [rot_post_volume] float  NULL,
    [rot_post_value] float  NULL,
    [rot_post_cost1] float  NULL,
    [rot_post_cost2] float  NULL,
    [rot_post_cost3] float  NULL,
    [rot_post_cost4] float  NULL,
    [rot_post_cost5] float  NULL,
    [rot_post_cost6] float  NULL,
    [rot_post_cost7] float  NULL,
    [rot_post_cost8] float  NULL,
    [rot_post_cost9] float  NULL,
    [rot_post_netto] float  NULL,
    [rot_post_origin] smallint  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_sample_trees_table'
CREATE TABLE [dbo].[esti_trakt_sample_trees_table] (
    [ttree_id] int  NOT NULL,
    [ttree_trakt_id] int  NOT NULL,
    [ttree_plot_id] int  NULL,
    [ttree_spc_id] int  NULL,
    [ttree_spc_name] nvarchar(20)  NULL,
    [ttree_dbh] float  NULL,
    [ttree_hgt] float  NULL,
    [ttree_gcrown] float  NULL,
    [ttree_bark_thick] float  NULL,
    [ttree_grot] float  NULL,
    [ttree_age] int  NULL,
    [ttree_growth] int  NULL,
    [ttree_tree_type] int  NULL,
    [ttree_dcls_from] float  NULL,
    [ttree_dcls_to] float  NULL,
    [ttree_m3sk] float  NULL,
    [ttree_m3fub] float  NULL,
    [ttree_m3ub] float  NULL,
    [ttree_bonitet] nvarchar(10)  NULL,
    [created] datetime  NOT NULL,
    [ttree_coord] nvarchar(80)  NULL
);
GO

-- Creating table 'esti_trakt_set_spc_table'
CREATE TABLE [dbo].[esti_trakt_set_spc_table] (
    [tsetspc_id] int  NOT NULL,
    [tsetspc_tdata_id] int  NOT NULL,
    [tsetspc_tdata_trakt_id] int  NOT NULL,
    [tsetspc_data_type] smallint  NOT NULL,
    [tsetspc_specie_id] int  NULL,
    [tsetspc_hgt_func_id] int  NULL,
    [tsetspc_hgt_specie_id] int  NULL,
    [tsetspc_hgt_func_index] int  NULL,
    [tsetspc_vol_func_id] int  NULL,
    [tsetspc_vol_specie_id] int  NULL,
    [tsetspc_vol_func_index] int  NULL,
    [tsetspc_bark_func_id] int  NULL,
    [tsetspc_bark_specie_id] int  NULL,
    [tsetspc_bark_func_index] int  NULL,
    [tsetspc_vol_ub_func_id] int  NULL,
    [tsetspc_vol_ub_specie_id] int  NULL,
    [tsetspc_vol_ub_func_index] int  NULL,
    [tsetspc_m3sk_m3ub] float  NULL,
    [tsetspc_m3sk_m3fub] float  NULL,
    [tsetspc_m3fub_m3to] float  NULL,
    [tsetspc_dcls] float  NULL,
    [tsetspc_qdesc_index] int  NULL,
    [tsetspc_extra_info] nvarchar(50)  NULL,
    [created] datetime  NOT NULL,
    [tsetspc_transp_dist] int  NULL,
    [tsetspc_transp_dist2] int  NULL,
    [tsetspc_qdesc_name] nvarchar(50)  NULL,
    [tsetspc_grot_func_id] int  NULL,
    [tsetspc_grot_percent] float  NULL,
    [tsetspc_grot_price] float  NULL,
    [tsetspc_grot_cost] float  NULL
);
GO

-- Creating table 'esti_trakt_spc_assort_table'
CREATE TABLE [dbo].[esti_trakt_spc_assort_table] (
    [tass_assort_id] int  NOT NULL,
    [tass_trakt_id] int  NOT NULL,
    [tass_trakt_data_id] int  NOT NULL,
    [tass_data_type] smallint  NOT NULL,
    [tass_assort_name] nvarchar(31)  NULL,
    [tass_price_m3fub] float  NULL,
    [tass_price_m3to] float  NULL,
    [tass_m3fub] float  NULL,
    [tass_m3to] float  NULL,
    [tass_m3fub_value] float  NULL,
    [tass_m3to_value] float  NULL,
    [created] datetime  NOT NULL,
    [tass_kr_m3_value] float  NULL
);
GO

-- Creating table 'esti_trakt_table'
CREATE TABLE [dbo].[esti_trakt_table] (
    [trakt_id] int IDENTITY(1,1) NOT NULL,
    [trakt_num] nvarchar(20)  NULL,
    [trakt_pnum] nvarchar(10)  NULL,
    [trakt_name] nvarchar(50)  NULL,
    [trakt_type] nvarchar(15)  NULL,
    [trakt_date] nvarchar(15)  NULL,
    [trakt_prop_id] int  NULL,
    [trakt_areal] float  NULL,
    [trakt_areal_consider] float  NULL,
    [trakt_areal_handled] float  NULL,
    [trakt_age] int  NULL,
    [trakt_si_h100] nvarchar(10)  NULL,
    [trakt_withdraw] float  NULL,
    [trakt_hgt_over_sea] int  NULL,
    [trakt_ground] int  NULL,
    [trakt_surface] int  NULL,
    [trakt_rake] int  NULL,
    [trakt_mapdata] nvarchar(40)  NULL,
    [trakt_latitude] int  NULL,
    [trakt_longitude] int  NULL,
    [trakt_x_coord] nvarchar(25)  NULL,
    [trakt_y_coord] nvarchar(25)  NULL,
    [trakt_origin] nvarchar(30)  NULL,
    [trakt_inp_method] nvarchar(30)  NULL,
    [trakt_hgt_class] nvarchar(30)  NULL,
    [trakt_cut_class] nvarchar(30)  NULL,
    [trakt_sim_data] int  NULL,
    [trakt_near_coast] int  NULL,
    [trakt_southeast] int  NULL,
    [trakt_region5] int  NULL,
    [trakt_part_of_plot] int  NULL,
    [trakt_si_h100_pine] nvarchar(10)  NULL,
    [trakt_prop_num] nvarchar(50)  NULL,
    [trakt_prop_name] nvarchar(50)  NULL,
    [trakt_notes] nvarchar(max)  NULL,
    [trakt_created_by] nvarchar(100)  NULL,
    [created] datetime  NOT NULL,
    [trakt_wside] tinyint  NULL,
    [trakt_img] varbinary(max)  NULL,
    [trakt_areal_factor] float  NULL,
    [trakt_hgtcurve] varbinary(max)  NULL,
    [trakt_coordinates] varchar(max)  NULL,
    [trakt_for_stand_only] smallint  NULL,
    [trakt_point] nvarchar(80)  NULL,
    [trakt_web_diarienr] nvarchar(25)  NULL,
    [trakt_web_bestandsnamn] nvarchar(50)  NULL
);
GO

-- Creating table 'esti_trakt_trans_assort_table'
CREATE TABLE [dbo].[esti_trakt_trans_assort_table] (
    [ttrans_trakt_data_id] int  NOT NULL,
    [ttrans_trakt_id] int  NOT NULL,
    [ttrans_from_ass_id] int  NOT NULL,
    [ttrans_to_ass_id] int  NOT NULL,
    [ttrans_to_spc_id] int  NOT NULL,
    [ttrans_data_type] smallint  NOT NULL,
    [ttrans_from_name] nvarchar(31)  NULL,
    [ttrans_to_name] nvarchar(31)  NULL,
    [ttrans_spc_name] nvarchar(31)  NULL,
    [ttrans_m3fub] float  NULL,
    [ttrans_percent] float  NULL,
    [created] datetime  NOT NULL,
    [ttrans_trans_m3] float  NULL
);
GO

-- Creating table 'esti_trakt_trees_assort_table'
CREATE TABLE [dbo].[esti_trakt_trees_assort_table] (
    [tassort_tree_trakt_id] int  NOT NULL,
    [tassort_tree_id] int  NOT NULL,
    [tassort_tree_dcls] float  NOT NULL,
    [tassort_spc_id] int  NULL,
    [tassort_spc_name] nvarchar(30)  NULL,
    [tassort_assort_name] nvarchar(30)  NULL,
    [tassort_exch_volume] float  NULL,
    [tassort_exch_price] float  NULL,
    [tassort_price_in] int  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'esti_trakt_type_table'
CREATE TABLE [dbo].[esti_trakt_type_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [type_name] nvarchar(45)  NULL,
    [notes] nvarchar(255)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'fst_categories_for_contacts_table'
CREATE TABLE [dbo].[fst_categories_for_contacts_table] (
    [contact_id] int  NOT NULL,
    [category_id] int  NOT NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'fst_category_table'
CREATE TABLE [dbo].[fst_category_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [category] nvarchar(45)  NULL,
    [notes] nvarchar(255)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'fst_contacts_table'
CREATE TABLE [dbo].[fst_contacts_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [pnr_orgnr] nvarchar(50)  NULL,
    [name_of] nvarchar(50)  NULL,
    [company] nvarchar(50)  NULL,
    [address] nvarchar(50)  NULL,
    [post_num] nvarchar(10)  NULL,
    [post_address] nvarchar(60)  NULL,
    [county] nvarchar(40)  NULL,
    [country] nvarchar(40)  NULL,
    [phone_work] nvarchar(25)  NULL,
    [phone_home] nvarchar(25)  NULL,
    [fax_number] nvarchar(25)  NULL,
    [mobile] nvarchar(30)  NULL,
    [e_mail] nvarchar(50)  NULL,
    [web_site] nvarchar(50)  NULL,
    [notes] nvarchar(max)  NULL,
    [created_by] nvarchar(100)  NULL,
    [created] datetime  NOT NULL,
    [vat_number] nvarchar(40)  NULL,
    [connect_id] int  NULL,
    [type_of] int  NULL,
    [picture] varbinary(max)  NULL,
    [co_address] nvarchar(50)  NULL,
    [bankgiro] nvarchar(40)  NULL,
    [plusgiro] nvarchar(40)  NULL,
    [bankkonto] nvarchar(40)  NULL,
    [levnummer] nvarchar(40)  NULL,
    [avinummer] nvarchar(40)  NULL
);
GO

-- Creating table 'fst_county_municipal_parish_table'
CREATE TABLE [dbo].[fst_county_municipal_parish_table] (
    [county_id] int  NOT NULL,
    [municipal_id] int  NOT NULL,
    [parish_id] int  NOT NULL,
    [county_name] nvarchar(31)  NULL,
    [municipal_name] nvarchar(31)  NULL,
    [parish_name] nvarchar(31)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'fst_external_doc_table'
CREATE TABLE [dbo].[fst_external_doc_table] (
    [extdoc_id] int IDENTITY(1,1) NOT NULL,
    [extdoc_link_tbl] nvarchar(127)  NOT NULL,
    [extdoc_link_id] int  NULL,
    [extdoc_link_type] tinyint  NULL,
    [extdoc_url] nvarchar(255)  NULL,
    [extdoc_note] nvarchar(127)  NULL,
    [extdoc_created_by] nvarchar(100)  NULL,
    [extdoc_created] nvarchar(20)  NULL
);
GO

-- Creating table 'fst_postnumber_table'
CREATE TABLE [dbo].[fst_postnumber_table] (
    [id] int  NOT NULL,
    [postnumber] nvarchar(45)  NULL,
    [postname] nvarchar(45)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'fst_prop_owner_table'
CREATE TABLE [dbo].[fst_prop_owner_table] (
    [prop_id] int  NOT NULL,
    [contact_id] int  NOT NULL,
    [owner_share] nvarchar(10)  NULL,
    [is_contact] smallint  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'fst_property_table'
CREATE TABLE [dbo].[fst_property_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [county_code] nvarchar(5)  NULL,
    [municipal_code] nvarchar(5)  NULL,
    [parish_code] nvarchar(5)  NULL,
    [county_name] nvarchar(31)  NULL,
    [municipal_name] nvarchar(31)  NULL,
    [parish_name] nvarchar(31)  NULL,
    [prop_number] nvarchar(20)  NULL,
    [prop_name] nvarchar(40)  NULL,
    [block_number] nvarchar(5)  NULL,
    [unit_number] nvarchar(5)  NULL,
    [areal_ha] float  NULL,
    [areal_measured_ha] float  NULL,
    [created_by] nvarchar(20)  NULL,
    [created] datetime  NOT NULL,
    [obj_id] nvarchar(30)  NULL,
    [prop_full_name] nvarchar(40)  NULL,
    [mottagarref] nvarchar(40)  NULL
);
GO

-- Creating table 'fst_species_table'
CREATE TABLE [dbo].[fst_species_table] (
    [spc_id] int  NOT NULL,
    [spc_name] nvarchar(45)  NULL,
    [notes] nvarchar(255)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'prn_pricelist_table'
CREATE TABLE [dbo].[prn_pricelist_table] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(50)  NULL,
    [type_of] int  NULL,
    [pricelist] nvarchar(max)  NULL,
    [created_by] nvarchar(100)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'tmpl_template_table'
CREATE TABLE [dbo].[tmpl_template_table] (
    [tmpl_id] int IDENTITY(1,1) NOT NULL,
    [tmpl_name] nvarchar(50)  NULL,
    [tmpl_type_of] int  NULL,
    [tmpl_template] nvarchar(max)  NULL,
    [tmpl_notes] nvarchar(max)  NULL,
    [created_by] nvarchar(100)  NULL,
    [created] datetime  NOT NULL
);
GO

-- Creating table 'web_inventerare'
CREATE TABLE [dbo].[web_inventerare] (
    [inventerare_id] int IDENTITY(1,1) NOT NULL,
    [inventerare_namn] nvarchar(50)  NOT NULL,
    [inventerare_efternamn] nvarchar(50)  NOT NULL,
    [created] datetime  NOT NULL,
    [created_by] nvarchar(100)  NULL
);
GO

-- Creating table 'web_TraktInventerareSet'
CREATE TABLE [dbo].[web_TraktInventerareSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [namn] nvarchar(max)  NOT NULL,
    [efternamn] nvarchar(max)  NOT NULL,
    [esti_trakt_table_trakt_id] int  NOT NULL
);
GO

-- Creating table 'cost_template_table'
CREATE TABLE [dbo].[cost_template_table] (
    [cost_id] int IDENTITY(1,1) NOT NULL,
    [cost_name] nvarchar(50)  NULL,
    [cost_type_of] int  NULL,
    [cost_template] nvarchar(max)  NULL,
    [cost_notes] nvarchar(max)  NULL,
    [created_by] nvarchar(100)  NULL,
    [created] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id], [cutcls_name], [create_date] in table 'esti_cutcls_table'
ALTER TABLE [dbo].[esti_cutcls_table]
ADD CONSTRAINT [PK_esti_cutcls_table]
    PRIMARY KEY CLUSTERED ([id], [cutcls_name], [create_date] ASC);
GO

-- Creating primary key on [id], [hgtcls_name], [create_date] in table 'esti_hgtcls_table'
ALTER TABLE [dbo].[esti_hgtcls_table]
ADD CONSTRAINT [PK_esti_hgtcls_table]
    PRIMARY KEY CLUSTERED ([id], [hgtcls_name], [create_date] ASC);
GO

-- Creating primary key on [id], [inpdata_name], [create_date] in table 'esti_inpdata_table'
ALTER TABLE [dbo].[esti_inpdata_table]
ADD CONSTRAINT [PK_esti_inpdata_table]
    PRIMARY KEY CLUSTERED ([id], [inpdata_name], [create_date] ASC);
GO

-- Creating primary key on [id], [origin_name], [create_date] in table 'esti_origin_table'
ALTER TABLE [dbo].[esti_origin_table]
ADD CONSTRAINT [PK_esti_origin_table]
    PRIMARY KEY CLUSTERED ([id], [origin_name], [create_date] ASC);
GO

-- Creating primary key on [tdata_id], [tdata_trakt_id], [tdata_data_type] in table 'esti_trakt_data_table'
ALTER TABLE [dbo].[esti_trakt_data_table]
ADD CONSTRAINT [PK_esti_trakt_data_table]
    PRIMARY KEY CLUSTERED ([tdata_id], [tdata_trakt_id], [tdata_data_type] ASC);
GO

-- Creating primary key on [tdcls_id], [tdcls_trakt_id] in table 'esti_trakt_dcls_trees_table'
ALTER TABLE [dbo].[esti_trakt_dcls_trees_table]
ADD CONSTRAINT [PK_esti_trakt_dcls_trees_table]
    PRIMARY KEY CLUSTERED ([tdcls_id], [tdcls_trakt_id] ASC);
GO

-- Creating primary key on [tdcls1_id], [tdcls1_trakt_id] in table 'esti_trakt_dcls1_trees_table'
ALTER TABLE [dbo].[esti_trakt_dcls1_trees_table]
ADD CONSTRAINT [PK_esti_trakt_dcls1_trees_table]
    PRIMARY KEY CLUSTERED ([tdcls1_id], [tdcls1_trakt_id] ASC);
GO

-- Creating primary key on [tdcls2_id], [tdcls2_trakt_id] in table 'esti_trakt_dcls2_trees_table'
ALTER TABLE [dbo].[esti_trakt_dcls2_trees_table]
ADD CONSTRAINT [PK_esti_trakt_dcls2_trees_table]
    PRIMARY KEY CLUSTERED ([tdcls2_id], [tdcls2_trakt_id] ASC);
GO

-- Creating primary key on [tprl_trakt_id] in table 'esti_trakt_misc_data_table'
ALTER TABLE [dbo].[esti_trakt_misc_data_table]
ADD CONSTRAINT [PK_esti_trakt_misc_data_table]
    PRIMARY KEY CLUSTERED ([tprl_trakt_id] ASC);
GO

-- Creating primary key on [tplot_id], [tplot_trakt_id] in table 'esti_trakt_plot_table'
ALTER TABLE [dbo].[esti_trakt_plot_table]
ADD CONSTRAINT [PK_esti_trakt_plot_table]
    PRIMARY KEY CLUSTERED ([tplot_id], [tplot_trakt_id] ASC);
GO

-- Creating primary key on [otcrot_id], [otcrot_trakt_id] in table 'esti_trakt_rotpost_other_table'
ALTER TABLE [dbo].[esti_trakt_rotpost_other_table]
ADD CONSTRAINT [PK_esti_trakt_rotpost_other_table]
    PRIMARY KEY CLUSTERED ([otcrot_id], [otcrot_trakt_id] ASC);
GO

-- Creating primary key on [spcrot_id], [spcrot_trakt_id] in table 'esti_trakt_rotpost_spc_table'
ALTER TABLE [dbo].[esti_trakt_rotpost_spc_table]
ADD CONSTRAINT [PK_esti_trakt_rotpost_spc_table]
    PRIMARY KEY CLUSTERED ([spcrot_id], [spcrot_trakt_id] ASC);
GO

-- Creating primary key on [rot_trakt_id] in table 'esti_trakt_rotpost_table'
ALTER TABLE [dbo].[esti_trakt_rotpost_table]
ADD CONSTRAINT [PK_esti_trakt_rotpost_table]
    PRIMARY KEY CLUSTERED ([rot_trakt_id] ASC);
GO

-- Creating primary key on [ttree_id], [ttree_trakt_id] in table 'esti_trakt_sample_trees_table'
ALTER TABLE [dbo].[esti_trakt_sample_trees_table]
ADD CONSTRAINT [PK_esti_trakt_sample_trees_table]
    PRIMARY KEY CLUSTERED ([ttree_id], [ttree_trakt_id] ASC);
GO

-- Creating primary key on [tsetspc_id], [tsetspc_tdata_id], [tsetspc_tdata_trakt_id], [tsetspc_data_type] in table 'esti_trakt_set_spc_table'
ALTER TABLE [dbo].[esti_trakt_set_spc_table]
ADD CONSTRAINT [PK_esti_trakt_set_spc_table]
    PRIMARY KEY CLUSTERED ([tsetspc_id], [tsetspc_tdata_id], [tsetspc_tdata_trakt_id], [tsetspc_data_type] ASC);
GO

-- Creating primary key on [tass_assort_id], [tass_trakt_id], [tass_trakt_data_id], [tass_data_type] in table 'esti_trakt_spc_assort_table'
ALTER TABLE [dbo].[esti_trakt_spc_assort_table]
ADD CONSTRAINT [PK_esti_trakt_spc_assort_table]
    PRIMARY KEY CLUSTERED ([tass_assort_id], [tass_trakt_id], [tass_trakt_data_id], [tass_data_type] ASC);
GO

-- Creating primary key on [trakt_id] in table 'esti_trakt_table'
ALTER TABLE [dbo].[esti_trakt_table]
ADD CONSTRAINT [PK_esti_trakt_table]
    PRIMARY KEY CLUSTERED ([trakt_id] ASC);
GO

-- Creating primary key on [ttrans_trakt_data_id], [ttrans_trakt_id], [ttrans_from_ass_id], [ttrans_to_ass_id], [ttrans_to_spc_id], [ttrans_data_type] in table 'esti_trakt_trans_assort_table'
ALTER TABLE [dbo].[esti_trakt_trans_assort_table]
ADD CONSTRAINT [PK_esti_trakt_trans_assort_table]
    PRIMARY KEY CLUSTERED ([ttrans_trakt_data_id], [ttrans_trakt_id], [ttrans_from_ass_id], [ttrans_to_ass_id], [ttrans_to_spc_id], [ttrans_data_type] ASC);
GO

-- Creating primary key on [tassort_tree_trakt_id], [tassort_tree_id], [tassort_tree_dcls] in table 'esti_trakt_trees_assort_table'
ALTER TABLE [dbo].[esti_trakt_trees_assort_table]
ADD CONSTRAINT [PK_esti_trakt_trees_assort_table]
    PRIMARY KEY CLUSTERED ([tassort_tree_trakt_id], [tassort_tree_id], [tassort_tree_dcls] ASC);
GO

-- Creating primary key on [id] in table 'esti_trakt_type_table'
ALTER TABLE [dbo].[esti_trakt_type_table]
ADD CONSTRAINT [PK_esti_trakt_type_table]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [contact_id], [category_id] in table 'fst_categories_for_contacts_table'
ALTER TABLE [dbo].[fst_categories_for_contacts_table]
ADD CONSTRAINT [PK_fst_categories_for_contacts_table]
    PRIMARY KEY CLUSTERED ([contact_id], [category_id] ASC);
GO

-- Creating primary key on [id] in table 'fst_category_table'
ALTER TABLE [dbo].[fst_category_table]
ADD CONSTRAINT [PK_fst_category_table]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'fst_contacts_table'
ALTER TABLE [dbo].[fst_contacts_table]
ADD CONSTRAINT [PK_fst_contacts_table]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [county_id], [municipal_id], [parish_id] in table 'fst_county_municipal_parish_table'
ALTER TABLE [dbo].[fst_county_municipal_parish_table]
ADD CONSTRAINT [PK_fst_county_municipal_parish_table]
    PRIMARY KEY CLUSTERED ([county_id], [municipal_id], [parish_id] ASC);
GO

-- Creating primary key on [extdoc_id], [extdoc_link_tbl] in table 'fst_external_doc_table'
ALTER TABLE [dbo].[fst_external_doc_table]
ADD CONSTRAINT [PK_fst_external_doc_table]
    PRIMARY KEY CLUSTERED ([extdoc_id], [extdoc_link_tbl] ASC);
GO

-- Creating primary key on [id] in table 'fst_postnumber_table'
ALTER TABLE [dbo].[fst_postnumber_table]
ADD CONSTRAINT [PK_fst_postnumber_table]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [prop_id], [contact_id] in table 'fst_prop_owner_table'
ALTER TABLE [dbo].[fst_prop_owner_table]
ADD CONSTRAINT [PK_fst_prop_owner_table]
    PRIMARY KEY CLUSTERED ([prop_id], [contact_id] ASC);
GO

-- Creating primary key on [id] in table 'fst_property_table'
ALTER TABLE [dbo].[fst_property_table]
ADD CONSTRAINT [PK_fst_property_table]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [spc_id] in table 'fst_species_table'
ALTER TABLE [dbo].[fst_species_table]
ADD CONSTRAINT [PK_fst_species_table]
    PRIMARY KEY CLUSTERED ([spc_id] ASC);
GO

-- Creating primary key on [id] in table 'prn_pricelist_table'
ALTER TABLE [dbo].[prn_pricelist_table]
ADD CONSTRAINT [PK_prn_pricelist_table]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [tmpl_id] in table 'tmpl_template_table'
ALTER TABLE [dbo].[tmpl_template_table]
ADD CONSTRAINT [PK_tmpl_template_table]
    PRIMARY KEY CLUSTERED ([tmpl_id] ASC);
GO

-- Creating primary key on [inventerare_id] in table 'web_inventerare'
ALTER TABLE [dbo].[web_inventerare]
ADD CONSTRAINT [PK_web_inventerare]
    PRIMARY KEY CLUSTERED ([inventerare_id] ASC);
GO

-- Creating primary key on [Id] in table 'web_TraktInventerareSet'
ALTER TABLE [dbo].[web_TraktInventerareSet]
ADD CONSTRAINT [PK_web_TraktInventerareSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [cost_id] in table 'cost_template_table'
ALTER TABLE [dbo].[cost_template_table]
ADD CONSTRAINT [PK_cost_template_table]
    PRIMARY KEY CLUSTERED ([cost_id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [tass_trakt_data_id], [tass_trakt_id], [tass_data_type] in table 'esti_trakt_spc_assort_table'
ALTER TABLE [dbo].[esti_trakt_spc_assort_table]
ADD CONSTRAINT [fk_tass]
    FOREIGN KEY ([tass_trakt_data_id], [tass_trakt_id], [tass_data_type])
    REFERENCES [dbo].[esti_trakt_data_table]
        ([tdata_id], [tdata_trakt_id], [tdata_data_type])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tass'
CREATE INDEX [IX_fk_tass]
ON [dbo].[esti_trakt_spc_assort_table]
    ([tass_trakt_data_id], [tass_trakt_id], [tass_data_type]);
GO

-- Creating foreign key on [tdata_trakt_id] in table 'esti_trakt_data_table'
ALTER TABLE [dbo].[esti_trakt_data_table]
ADD CONSTRAINT [fk_tdata]
    FOREIGN KEY ([tdata_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tdata'
CREATE INDEX [IX_fk_tdata]
ON [dbo].[esti_trakt_data_table]
    ([tdata_trakt_id]);
GO

-- Creating foreign key on [tsetspc_tdata_id], [tsetspc_tdata_trakt_id], [tsetspc_data_type] in table 'esti_trakt_set_spc_table'
ALTER TABLE [dbo].[esti_trakt_set_spc_table]
ADD CONSTRAINT [fk_tsetspc1]
    FOREIGN KEY ([tsetspc_tdata_id], [tsetspc_tdata_trakt_id], [tsetspc_data_type])
    REFERENCES [dbo].[esti_trakt_data_table]
        ([tdata_id], [tdata_trakt_id], [tdata_data_type])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tsetspc1'
CREATE INDEX [IX_fk_tsetspc1]
ON [dbo].[esti_trakt_set_spc_table]
    ([tsetspc_tdata_id], [tsetspc_tdata_trakt_id], [tsetspc_data_type]);
GO

-- Creating foreign key on [ttrans_trakt_data_id], [ttrans_trakt_id], [ttrans_data_type] in table 'esti_trakt_trans_assort_table'
ALTER TABLE [dbo].[esti_trakt_trans_assort_table]
ADD CONSTRAINT [fk_ttrans]
    FOREIGN KEY ([ttrans_trakt_data_id], [ttrans_trakt_id], [ttrans_data_type])
    REFERENCES [dbo].[esti_trakt_data_table]
        ([tdata_id], [tdata_trakt_id], [tdata_data_type])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_ttrans'
CREATE INDEX [IX_fk_ttrans]
ON [dbo].[esti_trakt_trans_assort_table]
    ([ttrans_trakt_data_id], [ttrans_trakt_id], [ttrans_data_type]);
GO

-- Creating foreign key on [tassort_tree_id], [tassort_tree_trakt_id] in table 'esti_trakt_trees_assort_table'
ALTER TABLE [dbo].[esti_trakt_trees_assort_table]
ADD CONSTRAINT [fk_tassort]
    FOREIGN KEY ([tassort_tree_id], [tassort_tree_trakt_id])
    REFERENCES [dbo].[esti_trakt_dcls_trees_table]
        ([tdcls_id], [tdcls_trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [tdcls_trakt_id] in table 'esti_trakt_dcls_trees_table'
ALTER TABLE [dbo].[esti_trakt_dcls_trees_table]
ADD CONSTRAINT [fk_tdcls]
    FOREIGN KEY ([tdcls_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tdcls'
CREATE INDEX [IX_fk_tdcls]
ON [dbo].[esti_trakt_dcls_trees_table]
    ([tdcls_trakt_id]);
GO

-- Creating foreign key on [tdcls1_trakt_id] in table 'esti_trakt_dcls1_trees_table'
ALTER TABLE [dbo].[esti_trakt_dcls1_trees_table]
ADD CONSTRAINT [fk_tdcls1]
    FOREIGN KEY ([tdcls1_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tdcls1'
CREATE INDEX [IX_fk_tdcls1]
ON [dbo].[esti_trakt_dcls1_trees_table]
    ([tdcls1_trakt_id]);
GO

-- Creating foreign key on [tdcls2_trakt_id] in table 'esti_trakt_dcls2_trees_table'
ALTER TABLE [dbo].[esti_trakt_dcls2_trees_table]
ADD CONSTRAINT [fk_tdcls2]
    FOREIGN KEY ([tdcls2_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tdcls2'
CREATE INDEX [IX_fk_tdcls2]
ON [dbo].[esti_trakt_dcls2_trees_table]
    ([tdcls2_trakt_id]);
GO

-- Creating foreign key on [tprl_trakt_id] in table 'esti_trakt_misc_data_table'
ALTER TABLE [dbo].[esti_trakt_misc_data_table]
ADD CONSTRAINT [fk_tprl]
    FOREIGN KEY ([tprl_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [tplot_trakt_id] in table 'esti_trakt_plot_table'
ALTER TABLE [dbo].[esti_trakt_plot_table]
ADD CONSTRAINT [fk_tplot]
    FOREIGN KEY ([tplot_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_tplot'
CREATE INDEX [IX_fk_tplot]
ON [dbo].[esti_trakt_plot_table]
    ([tplot_trakt_id]);
GO

-- Creating foreign key on [otcrot_trakt_id] in table 'esti_trakt_rotpost_other_table'
ALTER TABLE [dbo].[esti_trakt_rotpost_other_table]
ADD CONSTRAINT [fk_otcrot]
    FOREIGN KEY ([otcrot_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_otcrot'
CREATE INDEX [IX_fk_otcrot]
ON [dbo].[esti_trakt_rotpost_other_table]
    ([otcrot_trakt_id]);
GO

-- Creating foreign key on [spcrot_trakt_id] in table 'esti_trakt_rotpost_spc_table'
ALTER TABLE [dbo].[esti_trakt_rotpost_spc_table]
ADD CONSTRAINT [fk_spcrot]
    FOREIGN KEY ([spcrot_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_spcrot'
CREATE INDEX [IX_fk_spcrot]
ON [dbo].[esti_trakt_rotpost_spc_table]
    ([spcrot_trakt_id]);
GO

-- Creating foreign key on [rot_trakt_id] in table 'esti_trakt_rotpost_table'
ALTER TABLE [dbo].[esti_trakt_rotpost_table]
ADD CONSTRAINT [fk_rot]
    FOREIGN KEY ([rot_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ttree_trakt_id] in table 'esti_trakt_sample_trees_table'
ALTER TABLE [dbo].[esti_trakt_sample_trees_table]
ADD CONSTRAINT [fk_ttree]
    FOREIGN KEY ([ttree_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_ttree'
CREATE INDEX [IX_fk_ttree]
ON [dbo].[esti_trakt_sample_trees_table]
    ([ttree_trakt_id]);
GO

-- Creating foreign key on [category_id] in table 'fst_categories_for_contacts_table'
ALTER TABLE [dbo].[fst_categories_for_contacts_table]
ADD CONSTRAINT [FK_Category]
    FOREIGN KEY ([category_id])
    REFERENCES [dbo].[fst_category_table]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Category'
CREATE INDEX [IX_FK_Category]
ON [dbo].[fst_categories_for_contacts_table]
    ([category_id]);
GO

-- Creating foreign key on [contact_id] in table 'fst_categories_for_contacts_table'
ALTER TABLE [dbo].[fst_categories_for_contacts_table]
ADD CONSTRAINT [FK_Contact]
    FOREIGN KEY ([contact_id])
    REFERENCES [dbo].[fst_contacts_table]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [contact_id] in table 'fst_prop_owner_table'
ALTER TABLE [dbo].[fst_prop_owner_table]
ADD CONSTRAINT [FK_Contact1]
    FOREIGN KEY ([contact_id])
    REFERENCES [dbo].[fst_contacts_table]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Contact1'
CREATE INDEX [IX_FK_Contact1]
ON [dbo].[fst_prop_owner_table]
    ([contact_id]);
GO

-- Creating foreign key on [prop_id] in table 'fst_prop_owner_table'
ALTER TABLE [dbo].[fst_prop_owner_table]
ADD CONSTRAINT [FK_Property]
    FOREIGN KEY ([prop_id])
    REFERENCES [dbo].[fst_property_table]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [esti_trakt_table_trakt_id] in table 'web_TraktInventerareSet'
ALTER TABLE [dbo].[web_TraktInventerareSet]
ADD CONSTRAINT [FK_esti_trakt_tableweb_TraktInventerare]
    FOREIGN KEY ([esti_trakt_table_trakt_id])
    REFERENCES [dbo].[esti_trakt_table]
        ([trakt_id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_esti_trakt_tableweb_TraktInventerare'
CREATE INDEX [IX_FK_esti_trakt_tableweb_TraktInventerare]
ON [dbo].[web_TraktInventerareSet]
    ([esti_trakt_table_trakt_id]);
GO

-- --------------------------------------------------
-- Adding default values
-- --------------------------------------------------

ALTER TABLE [dbo].[cost_template_table] ADD  CONSTRAINT [DF_cost_template_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_data_table] ADD  CONSTRAINT [DF_esti_trakt_data_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_dcls_trees_table] ADD  CONSTRAINT [DF_esti_trakt_dcls_trees_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_dcls1_trees_table] ADD  CONSTRAINT [DF_esti_trakt_dcls1_trees_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_dcls2_trees_table] ADD  CONSTRAINT [DF_esti_trakt_dcls2_trees_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_misc_data_table] ADD  CONSTRAINT [DF_esti_trakt_misc_data_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_plot_table] ADD  CONSTRAINT [DF_esti_trakt_plot_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_rotpost_other_table] ADD  CONSTRAINT [DF_esti_trakt_rotpost_other_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_rotpost_spc_table] ADD  CONSTRAINT [DF_esti_trakt_rotpost_spc_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_rotpost_table] ADD  CONSTRAINT [DF_esti_trakt_rotpost_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_sample_trees_table] ADD  CONSTRAINT [DF_esti_trakt_sample_trees_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_set_spc_table] ADD  CONSTRAINT [DF_esti_trakt_set_spc_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_spc_assort_table] ADD  CONSTRAINT [DF_esti_trakt_spc_assort_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_table] ADD  CONSTRAINT [DF_esti_trakt_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_trans_assort_table] ADD  CONSTRAINT [DF_esti_trakt_trans_assort_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_trees_assort_table] ADD  CONSTRAINT [DF_esti_trakt_trees_assort_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[esti_trakt_type_table] ADD  CONSTRAINT [DF_esti_trakt_type_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_categories_for_contacts_table] ADD  CONSTRAINT [DF_fst_categories_for_contacts_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_category_table] ADD  CONSTRAINT [DF_fst_category_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_contacts_table] ADD  CONSTRAINT [DF_fst_contacts_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_county_municipal_parish_table] ADD  CONSTRAINT [DF_fst_county_municipal_parish_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_postnumber_table] ADD  CONSTRAINT [DF_fst_postnumber_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_prop_owner_table] ADD  CONSTRAINT [DF_fst_prop_owner_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_property_table] ADD  CONSTRAINT [DF_fst_property_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[fst_species_table] ADD  CONSTRAINT [DF_fst_species_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[prn_pricelist_table] ADD  CONSTRAINT [DF_prn_pricelist_table_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[tmpl_template_table] ADD  CONSTRAINT [DF_tmpl_template_table_created]  DEFAULT (getdate()) FOR [created]
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------