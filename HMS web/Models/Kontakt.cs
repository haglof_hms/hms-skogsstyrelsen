﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HMS_web.Models
{
    [MetadataType(typeof(Kontakt))]
    public partial class fst_contacts_table
    {
    }

    public class Kontakt
    {
        public Kontakt()
        {
            created = DateTime.Now;
        }

        public object id { get; set; }
        [Display(Name="Person-/organisationsnummer")]
        public object pnr_orgnr { get; set; }
        [Display(Name = "Namn")]
        public object name_of { get; set; }
        [Display(Name = "Företag")]
        public object company { get; set; }
        [Display(Name = "Adress")]
        public object address { get; set; }
        [Display(Name = "Postnummer")]
        public object post_num { get; set; }
        [Display(Name = "Postadress")]
        public object post_address { get; set; }
        [Display(Name = "Region")]
        public string county { get; set; }
        [Display(Name = "Land")]
        public object country { get; set; }
        [Display(Name = "Telefonnummer arbete")]
        public object phone_work { get; set; }
        [Display(Name = "Telefonnummer hem")]
        public object phone_home { get; set; }
        [Display(Name = "Faxnummer")]
        public object fax_number { get; set; }
        [Display(Name = "Mobilnummer")]
        public object mobile { get; set; }
        [Display(Name = "Epostadress")]
        public object e_mail { get; set; }
        [Display(Name = "Hemsida")]
        public object web_site { get; set; }
        [Display(Name = "Anteckningar")]
        public object notes { get; set; }

        public object created_by { get; set; }
        public object created { get; set; }
        [Display(Name = "Momsnummer")]
        public object vat_number { get; set; }
        public object connect_id { get; set; }
        public object type_of { get; set; }
        public object picture { get; set; }
        [Display(Name = "C/O adress")]
        public object co_address { get; set; }

        [Display(Name = "Bankgiro")]
        public object bankgiro { get; set; }
        [Display(Name = "Plusgiro")]
        public object plusgiro { get; set; }
        [Display(Name = "Bankkonto")]
        public object bankkonto { get; set; }
        [Display(Name = "Leverantörsnummer")]
        public object levnummer { get; set; }
        [Display(Name = "Avinummer")]
        public object avinummer { get; set; }

        //public virtual ICollection<fst_categories_for_contacts_table> fst_categories_for_contacts_table { get; set; }
        //public virtual ICollection<fst_prop_owner_table> fst_prop_owner_table { get; set; }
    }
}