//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class esti_trakt_rotpost_other_table
    {
        public int otcrot_id { get; set; }
        public int otcrot_trakt_id { get; set; }
        public Nullable<double> otcrot_value { get; set; }
        public Nullable<double> otcrot_sum_value { get; set; }
        public string otcrot_text { get; set; }
        public Nullable<double> otcrot_percent { get; set; }
        public Nullable<short> otcrot_type { get; set; }
        public System.DateTime created { get; set; }
    
        public virtual esti_trakt_table esti_trakt_table { get; set; }
    }
}
