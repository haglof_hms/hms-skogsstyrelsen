//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class esti_trakt_spc_assort_table
    {
        public int tass_assort_id { get; set; }
        public int tass_trakt_id { get; set; }
        public int tass_trakt_data_id { get; set; }
        public short tass_data_type { get; set; }
        public string tass_assort_name { get; set; }
        public Nullable<double> tass_price_m3fub { get; set; }
        public Nullable<double> tass_price_m3to { get; set; }
        public Nullable<double> tass_m3fub { get; set; }
        public Nullable<double> tass_m3to { get; set; }
        public Nullable<double> tass_m3fub_value { get; set; }
        public Nullable<double> tass_m3to_value { get; set; }
        public System.DateTime created { get; set; }
        public Nullable<double> tass_kr_m3_value { get; set; }
    
        public virtual esti_trakt_data_table esti_trakt_data_table { get; set; }
    }
}
