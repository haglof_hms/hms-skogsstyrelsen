//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMS_web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class esti_trakt_sample_trees_table
    {
        public int ttree_id { get; set; }
        public int ttree_trakt_id { get; set; }
        public Nullable<int> ttree_plot_id { get; set; }
        public Nullable<int> ttree_spc_id { get; set; }
        public string ttree_spc_name { get; set; }
        public Nullable<double> ttree_dbh { get; set; }
        public Nullable<double> ttree_hgt { get; set; }
        public Nullable<double> ttree_gcrown { get; set; }
        public Nullable<double> ttree_bark_thick { get; set; }
        public Nullable<double> ttree_grot { get; set; }
        public Nullable<int> ttree_age { get; set; }
        public Nullable<int> ttree_growth { get; set; }
        public Nullable<int> ttree_tree_type { get; set; }
        public Nullable<double> ttree_dcls_from { get; set; }
        public Nullable<double> ttree_dcls_to { get; set; }
        public Nullable<double> ttree_m3sk { get; set; }
        public Nullable<double> ttree_m3fub { get; set; }
        public Nullable<double> ttree_m3ub { get; set; }
        public string ttree_bonitet { get; set; }
        public System.DateTime created { get; set; }
        public string ttree_coord { get; set; }
    
        public virtual esti_trakt_table esti_trakt_table { get; set; }
    }
}
