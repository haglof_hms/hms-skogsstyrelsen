﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_web.Models
{
    [MetadataType(typeof(esti_trakt_table_metadata))]
    public partial class esti_trakt_table
    {
    }

    public class esti_trakt_table_metadata
    {
        public object trakt_id { get; set; }
        public object trakt_num { get; set; }
        public object trakt_pnum { get; set; }
        //[Required(ErrorMessage = "Beståndsnamn måste fyllas i!")]
        [Display(Name = "Beståndsnamn")]
        public object trakt_name { get; set; }
        public object trakt_type { get; set; }
        [Display(Name = "Inventeringsdatum")]
        public object trakt_date { get; set; }
        public object trakt_prop_id { get; set; }
        [Display(Name = "Areal (Ha)")]
        public object trakt_areal { get; set; }
        public object trakt_areal_consider { get; set; }
        public object trakt_areal_handled { get; set; }
        [Display(Name = "Ålder")]
        public object trakt_age { get; set; }
        [Display(Name = "Ståndortsindex")]
        public object trakt_si_h100 { get; set; }
        public object trakt_withdraw { get; set; }
        public object trakt_hgt_over_sea { get; set; }
        public object trakt_ground { get; set; }
        public object trakt_surface { get; set; }
        public object trakt_rake { get; set; }
        public object trakt_mapdata { get; set; }
        public object trakt_latitude { get; set; }
        public object trakt_longitude { get; set; }
        public object trakt_x_coord { get; set; }
        public object trakt_y_coord { get; set; }
        public object trakt_origin { get; set; }
        public object trakt_inp_method { get; set; }
        public object trakt_hgt_class { get; set; }
        public object trakt_cut_class { get; set; }
        public object trakt_sim_data { get; set; }
        public object trakt_near_coast { get; set; }
        public object trakt_southeast { get; set; }
        public object trakt_region5 { get; set; }
        public object trakt_part_of_plot { get; set; }
        [Display(Name = "SI")]
        public object trakt_si_h100_pine { get; set; }
        [Display(Name = "Fastighetsnr.")]
        public object trakt_prop_num { get; set; }
        [Display(Name = "Fastighet")]
        public object trakt_prop_name { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name = "Anteckningar")]
        public object trakt_notes { get; set; }
        [Display(Name = "Skapad av")]
        public object trakt_created_by { get; set; }
        [Display(Name = "Datum skapad")]
        public object created { get; set; }
        public object trakt_wside { get; set; }
        public object trakt_img { get; set; }
        public object trakt_areal_factor { get; set; }
        public object trakt_hgtcurve { get; set; }
        public object trakt_coordinates { get; set; }
        public object trakt_for_stand_only { get; set; }
        public object trakt_point { get; set; }
        [Display(Name = "Diarienummer")]
        public object trakt_web_diarienr { get; set; }
        [Required(ErrorMessage = "Beståndsnamn måste fyllas i!")]
        [Display(Name = "Beståndsnamn")]
        public object trakt_web_bestandsnamn { get; set; }
    }


    public class InvFil
    {
        public string Filnamn { get; set; }
        public string LagratFilnamn { get; set; }
        private string Data { get; set; }

        public InvFil(string filnamn, string lagratFilnamn)
        {
            Filnamn = filnamn;
            LagratFilnamn = lagratFilnamn;

            //m_Data = FileContentResult.Open();
        }
    }

    public class Bestand
    {
        public Bestand()
        {
            Datum = DateTime.Now;

            InvFiler = new List<InvFil>();
        }

        public int BestandId { get; set; }
        public DateTime Datum { get; set; }
        public string Skapare { get; set; }

        [Required(ErrorMessage = "Minst en inventeringsfil måste väljas!")]
        [Display(Name = "Inventeringsfiler")]
        public string InvFil { get; set; }
        [Display(Name = "Inventeringsfiler")]
        public List<InvFil> InvFiler { get; set; }

        [Required(ErrorMessage = "Beståndsmall måste väljas!")]
        [Display(Name = "Beståndsmall")]
        public int BestandsmallId { get; set; }
        public IEnumerable<SelectListItem> BestandsmallList { get; set; }

        [Required(ErrorMessage = "Minst en inventerare måste väljas!")]
        [Display(Name = "Inventerare")]
        public List<int> InventerareIdList { get; set; }
        public IEnumerable<SelectListItem> InventerareList { get; set; }

        [Required(ErrorMessage = "Beståndsnamn måste fyllas i!")]
        [Display(Name = "Beståndsnamn")]
        public string Namn { get; set; }

        [Required(ErrorMessage = "Diarienummer måste fyllas i!")]
        [Display(Name = "Diarienummer")]
        public string DiarieNr { get; set; }

        [Display(Name = "Fastighetsnamn")]
        public string FastighetsNamn { get; set; }
        public int FastighetsId { get; set; }

        [Display(Name = "Fastighetsägare")]
        public string FastighetsAgare { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Anteckningar")]
        public string Anteckningar { get; set; }
    }

    public class BestandStamplingslangdViewModel
    {
        public esti_trakt_table trakt { get; set; }
        public Stamplingslangd stmpl { get; set; }
    }   
}
