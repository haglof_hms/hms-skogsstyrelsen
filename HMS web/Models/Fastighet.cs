﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HMS_web.Models
{
    [MetadataType(typeof(Fastighet))]
    public partial class fst_property_table
    {
    }

    public class Fastighet
    {
        public Fastighet()
        {
            created = DateTime.Now;
        }

        //public bool Error { get; set; }


        // Län
        [Display(Name="Länskod")]
        public object county_code { get; set; }
        [Display(Name = "Kommunkod")]
        public object municipal_code { get; set; }
        [Display(Name = "Församlingskod")]
        public object parish_code { get; set; }

        [Display(Name = "Län")]
        public object county_name { get; set; }
        [Display(Name = "Kommun")]
        public object municipal_name { get; set; }
        [Display(Name = "Församling")]
        public object parish_name { get; set; }

        [Display(Name = "Fastighetsnummer")]
        //[Required(ErrorMessage = "Fastighetsnummer måste fyllas i!")]
        public object prop_number { get; set; }
        [Display(Name = "Fastighetsnamn")]
        [Required(ErrorMessage = "Fastighetsnamn måste fyllas i!")]
        public object prop_name { get; set; }
        [Display(Name = "Block")]
        [Required(ErrorMessage = "Block måste fyllas i!")]
        public object block_number { get; set; }
        [Display(Name = "Enhet")]
        [Required(ErrorMessage = "Enhet måste fyllas i!")]
        public object unit_number { get; set; }
        [Display(Name = "Areal (Ha)")]
        public object areal_ha { get; set; }

        [Display(Name = "Skapad av")]
        public object created_by { get; set; }
        [Display(Name = "Skapad datum")]
        public object created { get; set; }
        [Display(Name = "Fastighetsnamn")]
        public object prop_full_name { get; set; }
    }
}