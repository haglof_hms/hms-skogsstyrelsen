﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HMS_web.Models
{
    [MetadataType(typeof(Inventerare))]
    public partial class web_inventerare
    {
    }

    public class Inventerare
    {
        public Inventerare()
        {
            created = DateTime.Now;
        }

        public object inventerare_id { get; set; }
        [Display(Name = "Namn")]
        [Required(ErrorMessage = "Namn måste fyllas i!")]
        public object inventerare_namn { get; set; }
        [Display(Name = "Efternamn")]
        [Required(ErrorMessage = "Efternamn måste fyllas i!")]
        public object inventerare_efternamn { get; set; }
        public object created { get; set; }
    }
}