﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_web.Classes
{
    public class EnhancedAuth : AuthorizeAttribute
    {
            private string _key;
            public EnhancedAuth(string key)
            {
                _key = key;
            }
            protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
            {
                Roles = ConfigurationManager.AppSettings[_key];
                return base.AuthorizeCore(httpContext);
            }
        }
}
