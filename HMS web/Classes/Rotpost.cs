﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
using HMS_web.Models;
using System.Data.SqlClient;
using System.Configuration;
using HMS_web.Classes;

namespace HMS_web.Classes
{
    public class Rotpost
    {
        public struct DB_CONNECTION_DATA
        {
            public Int32 nConn;
            public Int32 nAdminConn;
        }

        private string m_szMessages = new string(' ', 4000);
        public string GetMessages()
        {
            StringBuilder temp = new StringBuilder(m_szMessages.TrimEnd());
            temp.Length = temp.Length - 1;
            temp.Replace("\n", "<br>");

            return temp.ToString();
        }

        [DllImport(@"SQL-bridge.dll", SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 OpenConnection(ref DB_CONNECTION_DATA con, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szServer, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szDatabase, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szUser, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szPassword);

        [DllImport(@"SQL-bridge.dll", SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 CloseConnection(ref DB_CONNECTION_DATA con);

        [DllImport(@"SQL-bridge.dll", SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 ImportFile2([MarshalAs(UnmanagedType.VBByRefStr)] ref string file, [MarshalAs(UnmanagedType.VBByRefStr)] ref string templateXml, [MarshalAs(UnmanagedType.VBByRefStr)] ref string username, ref DB_CONNECTION_DATA con, ref Int32 traktId);

        [DllImport(@"SQL-bridge.dll", SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 calculateStand2(int trakt_id, ref DB_CONNECTION_DATA con);

        [DllImport(@"SQL-bridge.dll", SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 GetLogMessages([MarshalAs(UnmanagedType.VBByRefStr)] ref string pszMessages);

        
        public int Calculate(IEnumerable<InvFil> listFiles, string szTemplateXml, string szUsername, int nTraktId = -1)
        {
            DB_CONNECTION_DATA m_db;
            m_db.nAdminConn = 0;
            m_db.nConn = 0;
            //int nTraktId = -1;
            bool bError = false;

            String sError = "";

            try
            {
                // set current directory and PATH variable
                /*String sCurrentDirectory = "";
                Directory.SetCurrentDirectory("C:\\inetpub\\Kuben\\bin");   //("C:\\Sources\\w32\\vC\\HMS\\HMS Skogsstyrelsen\\Debug");  //TODO: Hämta ut sökvägen från webservern eller nått Server.MapPath("~/bin")
                sCurrentDirectory = Directory.GetCurrentDirectory();
                String sPath = System.Environment.GetEnvironmentVariable("PATH");
                sCurrentDirectory += (";" + sPath);
                System.Environment.SetEnvironmentVariable("PATH", sCurrentDirectory);
                */
               
                // open SQLAPI++ connection
                var db = new HMS_webEntities();
                HMS_webEntities context = new HMS_webEntities();
                SqlConnectionStringBuilder con = new SqlConnectionStringBuilder(context.Database.Connection.ConnectionString);
                string szServer = con.DataSource;
                string szDatabase = con.InitialCatalog;
                string szUser = con.UserID;
                string szPassword = con.Password;

                if (OpenConnection(ref m_db, ref szServer, ref szDatabase, ref szUser, ref szPassword) == -1)
                {
                    GetLogMessages(ref m_szMessages);

                    throw( new Exception("Open connection failed!"));
                    //sError += "Open connection failed!<br>";
                }

                // load the inventory data into UMIndata.dll
                //string tmpTemplateXml = new string(szTemplateXml);  //"<?xml version=\"1.0\" encoding=\"UTF-8\"?><template><template_name>Normalskog BD län</template_name><template_done_by>Jörgen Brännvall</template_done_by><template_date>2010-02-22</template_date><template_notes></template_notes><template_exchange id=\"1\" name=\"Matrisprislista\"/><template_pricelist id=\"2\" type_of=\"1\" name=\"Sveaskog 123 BD\"/><template_dcls>2,00</template_dcls><template_hgt_over_sea>100</template_hgt_over_sea><template_latitude>65</template_latitude><template_longitude>0</template_longitude><template_growth_area></template_growth_area><template_si_h100>T16</template_si_h100><template_costs_tmpl id=\"2\">Fast kostnad BD</template_costs_tmpl><template_at_coast>0</template_at_coast><template_south_east>0</template_south_east><template_region_5>0</template_region_5><template_part_of_plot>0</template_part_of_plot><template_si_h100_pine>T16</template_si_h100_pine><template_specie id=\"1\" name=\"Tall\"><template_specie_hgt id=\"1500\" spc_id=\"-1\" index=\"0\" spc_name=\"Övriga trädslag\" func_type=\"H25\" func_area=\"Hela landet\" func_desc=\"\"/><template_specie_vol id=\"1000\" spc_id=\"1\" index=\"0\" spc_name=\"Tall\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\"/><template_specie_bark id=\"2000\" spc_id=\"1\" index=\"1\" spc_name=\"Tall\" func_type=\"Jonsson och Ostlin\" func_area=\"Serie 2\" func_desc=\"2,0;0,1\"/><template_specie_vol_ub id=\"1100\" spc_id=\"1\" index=\"0\" spc_name=\"Tall\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\" m3sk_m3ub=\"0,000000\"/><template_specie_qdesc id=\"0\" qdesc_name=\"Kvalitetsbeskrivning 1\"/><template_specie_grot id=\"0\" percent=\"0,000000\" price=\"0,000000\" cost=\"0,000000\"/><template_transp_dist1>100</template_transp_dist1><template_transp_dist2>0</template_transp_dist2><template_specie_sk_to_fub>0,00</template_specie_sk_to_fub><template_specie_fub_to_to>0,88</template_specie_fub_to_to><template_specie_h25>16,0</template_specie_h25><template_specie_gcrown>55</template_specie_gcrown></template_specie><template_specie id=\"2\" name=\"Gran\"><template_specie_hgt id=\"1500\" spc_id=\"2\" index=\"0\" spc_name=\"Gran\" func_type=\"H25\" func_area=\"Hela landet\" func_desc=\"\"/><template_specie_vol id=\"1000\" spc_id=\"2\" index=\"0\" spc_name=\"Gran\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\"/><template_specie_bark id=\"2000\" spc_id=\"2\" index=\"0\" spc_name=\"Gran\" func_type=\"Jonsson och Ostlin\" func_area=\"Serie 1\" func_desc=\"4,5;0,035\"/><template_specie_vol_ub id=\"1100\" spc_id=\"2\" index=\"0\" spc_name=\"Gran\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\" m3sk_m3ub=\"0,000000\"/><template_specie_qdesc id=\"0\" qdesc_name=\"Kvalitetsbeskrivning 1\"/><template_specie_grot id=\"0\" percent=\"0,000000\" price=\"0,000000\" cost=\"0,000000\"/><template_transp_dist1>100</template_transp_dist1><template_transp_dist2>0</template_transp_dist2><template_specie_sk_to_fub>0,00</template_specie_sk_to_fub><template_specie_fub_to_to>0,88</template_specie_fub_to_to><template_specie_h25>16,0</template_specie_h25><template_specie_gcrown>75</template_specie_gcrown></template_specie><template_specie id=\"3\" name=\"Björk\"><template_specie_hgt id=\"1500\" spc_id=\"-1\" index=\"0\" spc_name=\"Övriga trädslag\" func_type=\"H25\" func_area=\"Hela landet\" func_desc=\"\"/><template_specie_vol id=\"1000\" spc_id=\"3\" index=\"0\" spc_name=\"Björk\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\"/><template_specie_bark id=\"2000\" spc_id=\"3\" index=\"1\" spc_name=\"Björk\" func_type=\"Jonsson och Ostlin\" func_area=\"Serie 2\" func_desc=\"0,0;0,14\"/><template_specie_vol_ub id=\"1100\" spc_id=\"3\" index=\"0\" spc_name=\"Björk\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\" m3sk_m3ub=\"0,000000\"/><template_specie_qdesc id=\"0\" qdesc_name=\"Kvalitetsbeskrivning 1\"/><template_specie_grot id=\"0\" percent=\"0,000000\" price=\"0,000000\" cost=\"0,000000\"/><template_transp_dist1>100</template_transp_dist1><template_transp_dist2>0</template_transp_dist2><template_specie_sk_to_fub>0,00</template_specie_sk_to_fub><template_specie_fub_to_to>0,88</template_specie_fub_to_to><template_specie_h25>16,0</template_specie_h25><template_specie_gcrown>45</template_specie_gcrown></template_specie><template_specie id=\"4\" name=\"Övrigt löv\"><template_specie_hgt id=\"1500\" spc_id=\"-1\" index=\"0\" spc_name=\"Övriga trädslag\" func_type=\"H25\" func_area=\"Hela landet\" func_desc=\"\"/><template_specie_vol id=\"1000\" spc_id=\"3\" index=\"0\" spc_name=\"Björk\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\"/><template_specie_bark id=\"2000\" spc_id=\"3\" index=\"1\" spc_name=\"Björk\" func_type=\"Jonsson och Ostlin\" func_area=\"Serie 2\" func_desc=\"0,0;0,14\"/><template_specie_vol_ub id=\"1100\" spc_id=\"3\" index=\"0\" spc_name=\"Björk\" func_type=\"Brandels\" func_area=\"Norra\" func_desc=\"Mindre\" m3sk_m3ub=\"0,000000\"/><template_specie_qdesc id=\"0\" qdesc_name=\"Kvalitetsbeskrivning 1\"/><template_specie_grot id=\"0\" percent=\"0,000000\" price=\"0,000000\" cost=\"0,000000\"/><template_transp_dist1>100</template_transp_dist1><template_transp_dist2>0</template_transp_dist2><template_specie_sk_to_fub>0,00</template_specie_sk_to_fub><template_specie_fub_to_to>0,88</template_specie_fub_to_to><template_specie_h25>16,0</template_specie_h25><template_specie_gcrown>45</template_specie_gcrown></template_specie></template>");
                //string tmpUsername = new string(szUsername); //"test-user");  //TODO: username of currently logged in user

                if (listFiles != null)
                {
                    foreach (InvFil inv in listFiles)
                    {
                        string szFile = inv.LagratFilnamn;
                        int nRet = ImportFile2(ref szFile, ref szTemplateXml, ref szUsername, ref m_db, ref nTraktId);
                        if (nRet == -1 || nRet == 0)
                        {
                            GetLogMessages(ref m_szMessages);
                            //sError += "Unable to create stand!<br>";
                            bError = true;
                            break;
                        }
                        else
                        {
                            // remove the inventory file
                            //System.IO.File.Delete(inv.LagratFilnamn);
                        }
                    }
                }

                if (bError == false && nTraktId > -1)
                {
                    // calculate the stand in UMEstimate.dll
                    int nRet = calculateStand2(nTraktId, ref m_db);
                    if (nRet == -1 || nRet == 0) bError = true;
                }
            }
            catch (Exception ex)
            {
                sError += "ERROR!<br>" + ex.ToString() + "<br>";
            }

            // close SQLAPI++ connection
            if (CloseConnection(ref m_db) == -1)
            {
                //sError += "Close connection failed!<br>";
            }

            GetLogMessages(ref m_szMessages);

            m_db.nConn = 0;
            m_db.nAdminConn = 0;

            if (bError == true)
                return -1;
            else
                return nTraktId;
        }
    }
}