﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace HMS_web.Classes
{
    public class HxlSpecies
    {
        public string Name;
        public int Number;

        public int DefH25;

        // utbyte
        public int BarkSerie;
        public int MinTimb;
        public int MinKlen;
        public int MinMass;
        public int G3m;
        public int PriceTim;
        public int PriceKlen;
        public int PriceMass;
        public int Cost;
    }

    public class HxlPlot
    {
        public int Id;
        public int Areal;
        public int Ohgt;
        public int DbhAge;
        public int Bonitet;
        public int SiTree;
        public int Si;
        public int Antal;
    }

    public class HxlTree
    {
        public int SpecNr;
        public int Dbh;
        public int HgtNum;
        public int Hgt;
        public int Hgt2;
        public int Age;
        public int Type;
        public int Bonitet;
        public int Plot;
    }

    public class InvfilHXL
    {
        // document
        public DateTime FileDate;
        public string OwnerId;
        public string Type;
        public string Ver;

        // objectData
        public string ProgramVersion;
        public string Tecken;
        public string Best;
        public int BestAge;
        public int Areal;
        public DateTime Date;
        public int Numsp;

        public List<HxlSpecies> Species;

        public int Numplots;
        public int Latitude;
        public int Metod;
        public int Bonitet;
        public int DbhAge;
        public int PartOf;
        public int Thinning;
        public int CoastDist;
        public int Grundfh;
        public int Ytstr;
        public int Lutn;

        // plot
        public List<HxlPlot> Plots;

        // tree
        public List<HxlTree> Trees;

        public InvfilHXL()
        {
            Species = new List<HxlSpecies>();
            Plots = new List<HxlPlot>();
            Trees = new List<HxlTree>();
        }

        public bool Parse(string hxlData)
        {
            try
            {
                string dateFormat = "yyyy-mm-dd";
                CultureInfo dateProvider = CultureInfo.InvariantCulture;

                byte[] byteArray = Encoding.UTF8.GetBytes(hxlData);
                MemoryStream stream = new MemoryStream(byteArray);
                XDocument xml = XDocument.Load(stream);
                IEnumerable<XElement> elements = xml.Elements();
                

                // HXL -> Document
                // HXL -> Object -> ObjectData
                // HXL -> Object -> PlotSet
                // HXL -> Object -> TreeSet

                IEnumerable<XElement> elemDocuments = elements.Descendants("Document");
                foreach (var element in elemDocuments)
                {
                    //element.
                }

                IEnumerable<XElement> elemObjects = elements.Descendants("Object");
                IEnumerable<XElement> elemObjectDatas = elemObjects.Descendants("ObjectData");
                foreach (var element in elemObjectDatas)
                {
                    //element.
                }

                IEnumerable<XElement> elemPlotSets = elemObjects.Descendants("PlotSet");
                foreach (var element in elemPlotSets)
                {
                    //element.
                }

                IEnumerable<XElement> elemTreeSets = elemObjects.Descendants("TreeSet");
                foreach (var element in elemTreeSets)
                {
                    //element.
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    }
}