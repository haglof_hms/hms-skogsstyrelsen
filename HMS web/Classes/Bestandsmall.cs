﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace HMS_web.Classes
{
    public class MallTradslag
    {
        public int Id { get; set; }
        public string Namn { get; set; }

        // höjdfunktion
        public int HojdfunktionId;
        public int HojdfunktionTradslagId;
        public int HojdfunktionIndex;
        public string HojdfunktionTradslagsnamn;
        public string HojdfunktionTyp;
        public string HojdfunktionArea;
        public string HojdfunktionBeskrivning;

        // volymsfunktion
        public int VolfunktionId;
        public int VolfunktionTradslagId;
        public int VolfunktionIndex;
        public string VolfunktionTradslagsnamn;
        public string VolfunktionTyp;
        public string VolfunktionArea;
        public string VolfunktionBeskrivning;

        // barkfunktion
        public int BarkfunktionId;
        public int BarkfunktionTradslagId;
        public int BarkfunktionIndex;
        public string BarkfunktionTradslagsnamn;
        public string BarkfunktionTyp;
        public string BarkfunktionArea;
        public string BarkfunktionBeskrivning;

        // volym under bark
        public int VolfunktionUbId;
        public int VolfunktionUbTradslagId;
        public int VolfunktionUbIndex;
        public string VolfunktionUbTradslagsnamn;
        public string VolfunktionUbTyp;
        public string VolfunktionUbArea;
        public string VolfunktionUbBeskrivning;
        public float VolfunktionUbM3skM3ub;

        // kvalitetbeskrivning
        public int KvalBeskId;
        public string KvalBeskNamn;

        // grot
        public int GrotId;
        public float GrotProcent;
        public float GrotPris;
        public float GrotKostnad;

        public int TransportDist1;
        public int TransportDist2;
        public float M3skToM3fub;
        public float M3fubToM3to;
        public float H25;
        public int Gronkrona;
    }

    public class Bestandsmall
    {
        [Display(Name = "Namn")]
        public string Namn { get; set; }
        [Display(Name = "Skapad av")]
        public string Skapare { get; set; }
        [Display(Name = "Datum skapad")]
        public DateTime SkapadDatum { get; set; }
        [Display(Name = "Datum skapad")]
        public string SkapadDatumText { get; set; }
        //[DataType(DataType.MultilineText)]
        [Display(Name = "Noteringar")]
        public string Noteringar { get; set; }
        public int UtbyteId { get; set; }
        public string UtbyteNamn { get; set; }
        public int PrislistId { get; set; }
        public int Prislisttyp { get; set; }
        public string Prislistnamn { get; set; }
        public float Diameterklass { get; set; }
        public int HojdOverHavet { get; set; }
        public int Latitud { get; set; }
        public int Longitud { get; set; }
        public string Tillvaxtomrade { get; set; }
        public string SiH100 { get; set; }

        public int KostnadsmallId { get; set; }
        public string KostnadsmallNamn { get; set; }

        public int Kustnara { get; set; }
        public int SydOstraSverige { get; set; }
        public int Region5 { get; set; }
        public int DelAvProvyta { get; set; }
        public string SiH100Tall { get; set; }

        public List<MallTradslag> Tradslag { get; set; }

        public Bestandsmall()
        {
            Tradslag = new List<MallTradslag>();
        }

        public bool ParseXml(string xmlData)
        {
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(xmlData);
                MemoryStream stream = new MemoryStream(byteArray);

                XDocument xml = XDocument.Load(stream);

                IEnumerable<XElement> elements = xml.Elements();


                //string dateFormat = "yyyy-mm-dd";
                //CultureInfo dateProvider = CultureInfo.InvariantCulture;

                foreach (var element in elements)
                {
                    Namn = element.Element("template_name").Value;
                    Skapare = element.Element("template_done_by").Value;
                    //SkapadDatum = DateTime.ParseExact(element.Element("template_date").Value, dateFormat, dateProvider);
                    SkapadDatumText = element.Element("template_date").Value;
                    Noteringar = (string)element.Element("template_notes");   // blir fel här?!
                    UtbyteId = int.Parse( element.Element("template_exchange").Attribute("id").Value );
                    UtbyteNamn = element.Element("template_exchange").Attribute("name").Value;
                    PrislistId = int.Parse(element.Element("template_pricelist").Attribute("id").Value);
                    Prislisttyp = int.Parse(element.Element("template_pricelist").Attribute("type_of").Value);
                    Prislistnamn = element.Element("template_pricelist").Attribute("name").Value;
                    //Diameterklass = float.Parse(element.Element("template_dcls").Value);
                    HojdOverHavet = int.Parse(element.Element("template_hgt_over_sea").Value);
                    Latitud = int.Parse(element.Element("template_latitude").Value);
                    Longitud = int.Parse(element.Element("template_longitude").Value);
                    Tillvaxtomrade = element.Element("template_growth_area").Value;
                    SiH100 = element.Element("template_si_h100").Value;
                    KostnadsmallId = int.Parse(element.Element("template_costs_tmpl").Attribute("id").Value);
                    KostnadsmallNamn = element.Element("template_costs_tmpl").Attribute("id").Value;
                    Kustnara = int.Parse(element.Element("template_at_coast").Value);
                    SydOstraSverige = int.Parse(element.Element("template_south_east").Value);
                    Region5 = int.Parse(element.Element("template_region_5").Value);
                    DelAvProvyta = int.Parse(element.Element("template_part_of_plot").Value);
                    SiH100Tall = element.Element("template_si_h100_pine").Value;

                    var elementSpecies = element.Descendants("template_specie");
                    foreach (var elementSpecie in elementSpecies)
                    {
                        MallTradslag mtsl = new MallTradslag();

                        mtsl.Id = int.Parse(elementSpecie.Attribute("id").Value);
                        mtsl.Namn = elementSpecie.Attribute("name").Value;

                        // höjdfunktion
                        mtsl.HojdfunktionId = int.Parse(elementSpecie.Element("template_specie_hgt").Attribute("id").Value);
                        mtsl.HojdfunktionTradslagId = int.Parse(elementSpecie.Element("template_specie_hgt").Attribute("spc_id").Value);
                        mtsl.HojdfunktionIndex = int.Parse(elementSpecie.Element("template_specie_hgt").Attribute("index").Value);
                        mtsl.HojdfunktionTradslagsnamn = elementSpecie.Element("template_specie_hgt").Attribute("spc_name").Value;
                        mtsl.HojdfunktionTyp = elementSpecie.Element("template_specie_hgt").Attribute("func_type").Value;
                        mtsl.HojdfunktionArea = elementSpecie.Element("template_specie_hgt").Attribute("func_area").Value;
                        mtsl.HojdfunktionBeskrivning = elementSpecie.Element("template_specie_hgt").Attribute("func_desc").Value;

                        // volymsfunktion
                        mtsl.VolfunktionId = int.Parse(elementSpecie.Element("template_specie_vol").Attribute("id").Value);
                        mtsl.VolfunktionTradslagId = int.Parse(elementSpecie.Element("template_specie_vol").Attribute("spc_id").Value);
                        mtsl.VolfunktionIndex = int.Parse(elementSpecie.Element("template_specie_vol").Attribute("index").Value);
                        mtsl.VolfunktionTradslagsnamn = elementSpecie.Element("template_specie_vol").Attribute("spc_name").Value;
                        mtsl.VolfunktionTyp = elementSpecie.Element("template_specie_vol").Attribute("func_type").Value;
                        mtsl.VolfunktionArea = elementSpecie.Element("template_specie_vol").Attribute("func_area").Value;
                        mtsl.VolfunktionBeskrivning = elementSpecie.Element("template_specie_vol").Attribute("func_desc").Value;

                        // barkfunktion
                        mtsl.BarkfunktionId = int.Parse(elementSpecie.Element("template_specie_bark").Attribute("id").Value);
                        mtsl.BarkfunktionTradslagId = int.Parse(elementSpecie.Element("template_specie_bark").Attribute("spc_id").Value);
                        mtsl.BarkfunktionIndex = int.Parse(elementSpecie.Element("template_specie_bark").Attribute("index").Value);
                        mtsl.BarkfunktionTradslagsnamn = elementSpecie.Element("template_specie_bark").Attribute("spc_name").Value;
                        mtsl.BarkfunktionTyp = elementSpecie.Element("template_specie_bark").Attribute("func_type").Value;
                        mtsl.BarkfunktionArea = elementSpecie.Element("template_specie_bark").Attribute("func_area").Value;
                        mtsl.BarkfunktionBeskrivning = elementSpecie.Element("template_specie_bark").Attribute("func_desc").Value;

                        // volym under bark
                        mtsl.VolfunktionUbId = int.Parse(elementSpecie.Element("template_specie_vol_ub").Attribute("id").Value);
                        mtsl.VolfunktionUbTradslagId = int.Parse(elementSpecie.Element("template_specie_vol_ub").Attribute("spc_id").Value);
                        mtsl.VolfunktionUbIndex = int.Parse(elementSpecie.Element("template_specie_vol_ub").Attribute("index").Value);
                        mtsl.VolfunktionUbTradslagsnamn = elementSpecie.Element("template_specie_vol_ub").Attribute("spc_name").Value;
                        mtsl.VolfunktionUbTyp = elementSpecie.Element("template_specie_vol_ub").Attribute("func_type").Value;
                        mtsl.VolfunktionUbArea = elementSpecie.Element("template_specie_vol_ub").Attribute("func_area").Value;
                        mtsl.VolfunktionUbBeskrivning = elementSpecie.Element("template_specie_vol_ub").Attribute("func_desc").Value;
                        //mtsl.VolfunktionUbM3skM3ub = float.Parse(elementSpecie.Element("template_specie_vol_ub").Attribute("m3sk_m3ub").Value);

                        mtsl.TransportDist1 = int.Parse(elementSpecie.Element("template_transp_dist1").Value);
                        mtsl.TransportDist2 = int.Parse(elementSpecie.Element("template_transp_dist2").Value);
                        //mtsl.M3skToM3fub = float.Parse(elementSpecie.Element("template_specie_sk_to_fub").Value);
                        //mtsl.M3fubToM3to = float.Parse(elementSpecie.Element("template_specie_fub_to_to").Value);
                        //mtsl.H25 = float.Parse(elementSpecie.Element("template_specie_h25").Value);
                        mtsl.Gronkrona = int.Parse(elementSpecie.Element("template_specie_gcrown").Value);

                        // kvalitetbeskrivning
                        mtsl.KvalBeskId = int.Parse(elementSpecie.Element("template_specie_qdesc").Attribute("id").Value);
                        mtsl.KvalBeskNamn = elementSpecie.Element("template_specie_qdesc").Attribute("qdesc_name").Value;

                        // grot
                        mtsl.GrotId = int.Parse(elementSpecie.Element("template_specie_grot").Attribute("id").Value);
                        //mtsl.GrotProcent = float.Parse(elementSpecie.Element("template_specie_grot").Attribute("percent").Value);
                        //mtsl.GrotPris = float.Parse(elementSpecie.Element("template_specie_grot").Attribute("price").Value);
                        //mtsl.GrotKostnad = float.Parse(elementSpecie.Element("template_specie_grot").Attribute("cost").Value);

                        Tradslag.Add(mtsl);
                    }
                }
            }
            catch (Exception e)
            {
                Noteringar = e.Message;
                return false;
            }

            return true;
        }
    }
}