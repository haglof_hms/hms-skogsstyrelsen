﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using HMS_web.Classes;
using System.Threading;

namespace HMS_web.Controllers
{
    public class MyFormViewModel
    {
        public HttpPostedFileBase file { get; set; }
    }

    public class BerakningController : Controller
    {
        private static Rotpost rp = new Rotpost();
//        private Object thisLock = new Object();

        public BerakningController()
        {
        }


        //
        // GET: /Berakning/

        public ActionResult Index()
        {
            MyFormViewModel fvm = new MyFormViewModel();
            return View(fvm);
        }


        //
        // POST: /Berakning/

        [HttpPost]
        [ValidateAntiForgeryToken]    // comment this out to test with wget
        public ActionResult Index(FormCollection collection)
        {
            MyFormViewModel fvm = new MyFormViewModel();
            TryUpdateModel<MyFormViewModel>(fvm);

            var sPath = "";
            List<string> listFiles = new List<string>();
            if (fvm.file != null)
            {
                if (fvm.file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(fvm.file.FileName);
                    sPath = Path.Combine(Server.MapPath("~/App_Data/uploads"), "(" + Guid.NewGuid() + ") " + fileName);
                    fvm.file.SaveAs(sPath);
                    listFiles.Add(sPath);
                }
            }

            // beräkna rotpost
            lock(rp)
            {
                //rp.Calculate(listFiles);
            }

            //TODO: Mata ut nån vettig sida
            return View();
        }
    }
}
