﻿using HMS_web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using HMS_web.Classes;
using System.IO;
using System.Xml.Linq;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Diagnostics;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel;
using System.Reflection;
using CrystalDecisions.Shared;
using System.Configuration;
using HMS_web.Infrastructure;

namespace HMS_web.Controllers
{
    public class PaginatedBestand<T> : PaginatedList<T>
    {
        // filtreringsargument
        public string Namn;
        public string Diarienr;
        public /*DateTime?*/ string DatumFran;
        public /*DateTime?*/ string DatumTill;
        public string Fastighet;
        public string Agare;
        public string Inventerare;
        public string SkapadAv;
        public string OrderBy;
        public int? OrderByDesc;
        public string DatumInvFran;
        public string DatumInvTill;

        public PaginatedBestand(IQueryable<T> source, int pageIndex, int pageSize) : base(source, pageIndex, pageSize)
        {
        }
    }

    public class BestandController : Controller
    {
        HMS_webEntities hmsDB = new HMS_webEntities();
        private static Rotpost rp = new Rotpost();

        //
        // GET: /Bestand/

        public ActionResult Index()
        {
            return View();
        }
        
        //
        // GET: /Bestand/Search

        public ActionResult Search(int? page, string namn, string diarienr, DateTime? datumFran, DateTime? datumTill, string orderBy, int? orderByDesc, string fastighet, string agare, string inventerare, string skapadAv, DateTime? datumInvFran, DateTime? datumInvTill, string reset = "")
        {
            const int pageSize = 10;

            if (reset == null || reset == "")
            {
                if (diarienr == null) diarienr = "";
                if (namn == null) namn = "";
                if (fastighet == null) fastighet = "";
                if (orderBy == null) orderBy = "";
                if (agare == null) agare = "";
                if (inventerare == null) inventerare = "";
                if (skapadAv == null) skapadAv = "";
            }
            else
            {
                ModelState.Clear();

                page = 0;
                datumFran = null;
                datumTill = null;
                diarienr = "";
                namn = "";
                fastighet = "";
                orderBy = "";
                agare = "";
                inventerare = "";
                skapadAv = "";
                datumInvFran = null;
                datumInvTill = null;
            }


            string sInvFran = "";
            string sInvTill = "";

            if(datumInvFran != null)
                sInvFran = ((DateTime)datumInvFran).ToString("yyyy-MM-dd");
            
            if(datumInvTill != null)
                sInvTill = ((DateTime)datumInvTill).ToString("yyyy-MM-dd");

            IQueryable<esti_trakt_table> dbTrakt;
            dbTrakt = from trakt in hmsDB.esti_trakt_table
                        join property in hmsDB.fst_property_table on trakt.trakt_prop_id equals property.id
                        where
                        (namn == "" || trakt.trakt_web_bestandsnamn.Contains(namn)) &&
                        (diarienr == "" || trakt.trakt_web_diarienr.Contains(diarienr)) &&
                        (fastighet == "" || trakt.trakt_prop_name.Contains(fastighet)) &&
                        (skapadAv == "" || trakt.trakt_created_by.Contains(skapadAv)) &&
                        (datumFran == null || trakt.created > datumFran) &&
                        (datumTill == null || trakt.created < datumTill) &&
                        (datumInvFran == null || trakt.trakt_date.CompareTo(sInvFran) >= 0) &&
                        (datumInvTill == null || trakt.trakt_date.CompareTo(sInvTill) <= 0)
                      select trakt;


            switch (orderBy)
            {
                case "trakt_prop_name":
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.trakt_prop_name);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.trakt_prop_name);
                    break;
                case "trakt_created_by":
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.trakt_created_by);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.trakt_created_by);
                    break;
                case "trakt_web_diarienr":
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.trakt_web_diarienr);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.trakt_web_diarienr);
                    break;
                case "trakt_areal":
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.trakt_areal);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.trakt_areal);
                    break;
                case "trakt_date":
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.trakt_date);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.trakt_date);
                    break;
                case "created":
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.created);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.created);
                    break;
                case "trakt_name":
                default:
                    if (orderByDesc == 1) dbTrakt = dbTrakt.OrderBy(a => a.trakt_web_bestandsnamn);
                    else dbTrakt = dbTrakt.OrderByDescending(a => a.trakt_web_bestandsnamn);
                    break;
            }            
            
            var paginatedTrakt = new PaginatedBestand<esti_trakt_table>(dbTrakt, page ?? 0, pageSize);

            paginatedTrakt.Diarienr = diarienr;
            paginatedTrakt.Namn = namn;
            paginatedTrakt.Fastighet = fastighet;
            paginatedTrakt.SkapadAv = skapadAv;
            paginatedTrakt.DatumFran = datumFran.ToString();
            paginatedTrakt.DatumTill = datumTill.ToString();
            paginatedTrakt.OrderBy = orderBy;
            paginatedTrakt.OrderByDesc = orderByDesc;
            paginatedTrakt.DatumInvFran = datumInvFran.ToString();
            paginatedTrakt.DatumInvTill = datumInvTill.ToString();

            return View(paginatedTrakt);
        }


        private void getBestandsmall(Bestand model)
        {
            model.BestandsmallList = new SelectList(hmsDB.tmpl_template_table.Where(x => x.tmpl_type_of != -1), "tmpl_id", "tmpl_name");
        }

        private void getInventerare(Bestand model)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            var queryInventerare = from i in hmsDB.web_inventerare
                                       .Where(a => model.InventerareIdList.Contains( a.inventerare_id ) )
                                       .OrderBy(a => a.inventerare_efternamn)
                                   select i;
            foreach (var i in queryInventerare)
            {
                listItems.Add(new SelectListItem
                {
                    Value = i.inventerare_id.ToString(),
                    Text = i.inventerare_efternamn + " " + i.inventerare_namn,
                    Selected = false
                });
            }
            model.InventerareList = new SelectList(listItems, "Value", "Text");
        }

        private void getFastighet(Bestand model)
        {
            /*var query = from i in hmsDB.fst_property_table
                                       .Where(a => a.id == model.FastighetsId)
                                   select i.prop_full_name.FirstOrDefault();
            if (query != null)
            {
                model.FastighetsNamn = query.First();
            }*/

/*            var query = hmsDB.fst_property_table.Where(a => a.id == model.FastighetsId).FirstOrDefault();
            if (query != null)
            {
                model.FastighetsNamn = query.First();
            }*/

            var query = hmsDB.fst_property_table.Where(a => a.id == model.FastighetsId).Select(a => a.prop_full_name);
            if (query != null)
            {
                model.FastighetsNamn = query.First();
            }
        }

        private void getTradslag(Stamplingslangd model)
        {
            List<Tradslag> listaTradslag = new List<Tradslag>();
            var query = hmsDB.esti_trakt_data_table.Where(x => x.tdata_trakt_id == model.nTraktId && x.tdata_data_type == 1).OrderBy(x => x.tdata_spc_id);
            foreach (var trslg in query)
            {
                Tradslag t = new Tradslag();
                t.nId = trslg.tdata_spc_id;
                t.sNamn = trslg.tdata_spc_name;

                listaTradslag.Add(t);
            }

            for (int i = 0; i < model.DiameterKlasser.Count(); i++ )
            {
                model.DiameterKlasser.ElementAt(i).Tradslagslista = new List<Tradslag>(listaTradslag);
            }
        }



        //
        // GET: /Bestand/Create

        public ActionResult Create()
        {
            Bestand model = new Bestand();

            // hämta ut listor
            getBestandsmall(model);
            
            model.InventerareList = new SelectList("", "Value", "Text");

            return View(model);
        }

        //
        // POST: /Bestand/Create

        [HttpPost]
        public ActionResult Create(Bestand bestand)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // hämta ut beståndsmallar till modell
                    getBestandsmall(bestand);
                    getInventerare(bestand);
                    getFastighet(bestand);

                    
                    // kontrollera att diarienummer inte redan finns i databasen
                    var query2 = hmsDB.esti_trakt_table.Where(a => a.trakt_web_diarienr == bestand.DiarieNr).FirstOrDefault();
                    if (query2 != null)  // record does exist, exit with an error
                    {
                        ModelState.AddModelError("DiarieNr", "Diarienummer finns redan!");
                        return View(bestand);
                    }


                    // användarnamn
                    string szUsername;
                    szUsername = User.Identity.Name;
                    bestand.Skapare = szUsername;


                    // spara ned inventeringsfiler(na)
                    bestand.InvFiler = new List<InvFil>();
                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        if (hpf.ContentLength == 0)
                            continue;

                        // IE8 bug: parsa ut bara filnamnet
                        string sTmpFilename = Path.GetFileName(hpf.FileName);

                        // ge inventeringsfilen ett unikt filnamn
                        string savedFileName = Path.Combine(Server.MapPath("~/App_Data/uploads"), "(" + Guid.NewGuid() + ") " + sTmpFilename);
                        hpf.SaveAs(savedFileName);
                        bestand.InvFiler.Add(new InvFil(hpf.FileName, savedFileName));
                    }


                    // hämta ut all data från vald beståndsmall och stoppa i en klass
                    string szTemplateXml;
                    var query = hmsDB.tmpl_template_table.Where(a => a.tmpl_id == bestand.BestandsmallId).Select(a => a.tmpl_template);
                    if (query != null)
                    {
                        szTemplateXml = query.First();
                        Bestandsmall bm = new Bestandsmall();
                        bm.ParseXml(szTemplateXml);
                    }
                    else
                    {
                        ModelState.AddModelError("BestandsmallId", "Beståndsmall saknas!");
                        return View(bestand);
                    }

                    // hämta ut trädslag från inventeringsfiler
                    /*foreach (InvFil inv in bestand.InvFiler)
                    {
                        XDocument xml = XDocument.Load(inv.LagratFilnamn);
                        xml.
                    }*/

                    // kolla att alla trädslag finns i både mall och inventeringsfiler


                    // Skapa/beräkna rotpost
                    int nRet = -1;
                    lock (rp)
                    {
                        String sCurrentDirectory = "";
                        Directory.SetCurrentDirectory(Server.MapPath("~/bin"));
                        sCurrentDirectory = Directory.GetCurrentDirectory();
                        String sPath = System.Environment.GetEnvironmentVariable("PATH");
                        sCurrentDirectory += (";" + sPath);
                        System.Environment.SetEnvironmentVariable("PATH", sCurrentDirectory);
                                                
                        nRet = rp.Calculate(bestand.InvFiler, szTemplateXml, szUsername);
                    }

                    if (nRet != -1)
                    {
                        bestand.BestandId = nRet;
                    }
                    else
                    {
                        ViewBag.Error = true;
                        ViewBag.ErrorMessage = rp.GetMessages();
                        return View(bestand);
                    }

                    // radera inventeringsfiler
                    foreach (InvFil inv in bestand.InvFiler)
                    {
                        System.IO.File.Delete(inv.LagratFilnamn);
                    }


                    try
                    {
                        // Uppdatera beståndsinformationen i databasen
                        esti_trakt_table bestDb = (from x in hmsDB.esti_trakt_table
                                                   where x.trakt_id == bestand.BestandId
                                                   select x).First();
                        bestDb.trakt_web_diarienr = bestand.DiarieNr;
                        bestDb.trakt_web_bestandsnamn = bestand.Namn;
                        bestDb.trakt_notes = bestand.Anteckningar;
                        bestDb.trakt_prop_id = bestand.FastighetsId;
                        bestDb.trakt_prop_num = hmsDB.fst_property_table.Where(x => x.id == bestDb.trakt_prop_id).Select(x => x.prop_number).First();
                        bestDb.trakt_prop_name = bestand.FastighetsNamn;

                        // Spara ned inventerare
                        web_inventerare inventerare;// = new web_inventerare();
                        bestDb.web_TraktInventerare = new List<web_TraktInventerare>();

                        foreach (int invId in bestand.InventerareIdList)
                        {
                            inventerare = hmsDB.web_inventerare.Where(x => x.inventerare_id == invId).First();
                            web_TraktInventerare traktInv = new web_TraktInventerare();
                            traktInv.namn = inventerare.inventerare_namn;
                            traktInv.efternamn = inventerare.inventerare_efternamn;
                            bestDb.web_TraktInventerare.Add(traktInv);
                        }
                        hmsDB.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }

                    // Visa detaljer för beståndet
                    return RedirectToAction("Details", new { id = bestand.BestandId });
                }
            }
            catch (Exception exp)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = exp.Message;
            }

            return View(bestand);
        }


        //
        // GET: /Bestand/Details/5

        public ActionResult Details(int id = 0)
        {
            esti_trakt_table trakt = hmsDB.esti_trakt_table.Find(id);
            if (trakt == null)
            {
                return HttpNotFound();
            }
            return View(trakt);
        }

        //
        // GET: /Bestand/Edit/5
        //[EnhancedAuth("adminGroup")]
        public ActionResult Edit(int id = 0)
        {
            esti_trakt_table traktNew = hmsDB.esti_trakt_table.Find(id);
            if (traktNew == null)
            {
                return HttpNotFound();
            }

            var stamplingslangd = new Stamplingslangd();
            stamplingslangd.nTraktId = id;
            stamplingslangd.DiameterIntervall = Convert.ToInt32(traktNew.esti_trakt_misc_data_table.tprl_dcls);

            List<Tradslag> listaTradslag = new List<Tradslag>();
            var query = hmsDB.esti_trakt_data_table.Where(x => x.tdata_trakt_id == id && x.tdata_data_type == 1).OrderBy(x => x.tdata_spc_id);
            foreach (var trslg in query)
            {
                Tradslag t = new Tradslag();
                t.nId = trslg.tdata_spc_id;
                t.sNamn = trslg.tdata_spc_name;

                listaTradslag.Add(t);
            }


            // hämta ut stämplingslängd (diameterklass)
            var query2 = hmsDB.esti_trakt_dcls_trees_table.Where(x => x.tdcls_trakt_id == id).OrderBy(x => x.tdcls_plot_id).ThenBy(x => x.tdcls_spc_id).ThenBy(x => x.tdcls_dcls_from);
            foreach (var tree in query2)
            {
                DiameterKlass d = new DiameterKlass();
                d.Yta = tree.tdcls_plot_id;
                d.TradslagId = tree.tdcls_spc_id;
                d.Tradslag = tree.tdcls_spc_name;
                d.Klass = Convert.ToInt32(tree.tdcls_dcls_from);
                d.Antal = Convert.ToInt32(tree.tdcls_numof);
                d.Tradslagslista = listaTradslag;

                stamplingslangd.DiameterKlasser.Add(d);
            }

            // hämta ut provträd
            var query3 = hmsDB.esti_trakt_sample_trees_table.Where(x => x.ttree_trakt_id == id).OrderBy(x => x.ttree_plot_id).ThenBy(x => x.ttree_spc_id);
            foreach (var stree in query3)
            {
                Provtrad ptrad = new Provtrad();
                ptrad.Yta = stree.ttree_plot_id;
                ptrad.Diameter = Convert.ToInt32(stree.ttree_dbh);
                ptrad.Hojd = Convert.ToInt32(stree.ttree_hgt);
                ptrad.Gronkronegrans = Convert.ToInt32(ptrad.Hojd * (stree.ttree_gcrown/100));  // konvertera % till dm
                ptrad.TradslagId = stree.ttree_spc_id;
                ptrad.Tradslag = stree.ttree_spc_name;
                ptrad.Tradslagslista = listaTradslag;
                ptrad.Alder = Convert.ToInt32(stree.ttree_age);
                ptrad.Barktjocklek = Convert.ToInt32(stree.ttree_bark_thick);

                stamplingslangd.Provtrad.Add(ptrad);
            }


            var ViewModel = new BestandStamplingslangdViewModel
            {
                trakt = traktNew,
                stmpl = stamplingslangd
            };

            return View(ViewModel);
        }

        //
        // POST: /Bestand/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[EnhancedAuth("adminGroup")]
        public ActionResult Edit(BestandStamplingslangdViewModel collection)
        {
            esti_trakt_table trakt = collection.trakt;
            getTradslag(collection.stmpl);


            if (ModelState.IsValid)
            {
                esti_trakt_table traktOld = hmsDB.esti_trakt_table.Find(trakt.trakt_id);
                if (traktOld == null)
                {
                    return HttpNotFound();
                }
                
                // kontrollera att diarienummer inte redan finns i databasen
                var query2 = hmsDB.esti_trakt_table.Where(a => a.trakt_web_diarienr == trakt.trakt_web_diarienr && a.trakt_id != trakt.trakt_id).FirstOrDefault();
                if (query2 != null)  // record does exist, exit with an error
                {
                    ModelState.AddModelError("trakt_web_diarienr", "Diarienummer finns redan!");
                    return View(collection);
                }


                // validera stämplingslängd
                // hämta ut startindex i ModelState för variablerna som är intressanta
                int nStartKey = 0;
                for (int k = 0; k < ModelState.Keys.Count(); k++)
                {
                    string sKey = ModelState.Keys.ElementAt(k);
                    if (sKey.Contains("DiameterKlasser"))
                    {
                        nStartKey = k;
                        break;
                    }
                }

                // kolla att stämplingslängdinformationen är riktigt ifylld
                bool bError = false;
                int diameterIntervall = Convert.ToInt32(traktOld.esti_trakt_misc_data_table.tprl_dcls);
                DiameterKlass dkls1, dkls2;
                for (int i = 0; i < collection.stmpl.DiameterKlasser.Count(); i++)
                {
                    dkls1 = collection.stmpl.DiameterKlasser.ElementAt(i);

                    // kolla att diameterklass är inom rätt område
                    if (diameterIntervall > 0)
                    {
                        if (dkls1.Klass % diameterIntervall != 0)
                        {
                            string sKey = ModelState.Keys.ElementAt(nStartKey + (i * 4));
                            ModelState.AddModelError(sKey, "Felaktig diameterklass!");
                            bError = true;
                        }
                    }

                    for (int j = i+1; j < collection.stmpl.DiameterKlasser.Count(); j++)
                    {
                        dkls2 = collection.stmpl.DiameterKlasser.ElementAt(j);

                        // kolla om det är samma yta, trädslag och diameterklass
                        if (dkls1.Yta == dkls2.Yta && dkls1.TradslagId == dkls2.TradslagId && dkls1.Klass == dkls2.Klass)
                        {
                            string sKey = ModelState.Keys.ElementAt(nStartKey + (j * 4));
                            ModelState.AddModelError(sKey, "Diameterklass finns redan för detta trädslaget!");
                            bError = true;
                        }
                    }
                }

                if (bError == true)
                {
                    return View(collection);
                }

                // spara ned stämplingslängd till databas
                List<Tradslag> listaTradslag = new List<Tradslag>();
                var query = hmsDB.esti_trakt_data_table.Where(x => x.tdata_trakt_id == traktOld.trakt_id && x.tdata_data_type == 1).OrderBy(x => x.tdata_spc_id);
                foreach (var trslg in query)
                {
                    Tradslag t = new Tradslag();
                    t.nId = trslg.tdata_spc_id;
                    t.sNamn = trslg.tdata_spc_name;

                    listaTradslag.Add(t);
                }

                // töm tabellen först
                var deleteOrderDetails =
                    from dbDkls in traktOld.esti_trakt_dcls_trees_table
                    where dbDkls.tdcls_trakt_id == traktOld.trakt_id
                    select dbDkls;

                foreach (var detail in deleteOrderDetails.Reverse())
                {
                    hmsDB.esti_trakt_dcls_trees_table.Remove(detail);
                }

                try
                {
                    hmsDB.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Provide for exceptions.
                }


                for (int i = 0; i < collection.stmpl.DiameterKlasser.Count(); i++)
                {
                    dkls1 = collection.stmpl.DiameterKlasser.ElementAt(i);

                    esti_trakt_dcls_trees_table dbDkls = new esti_trakt_dcls_trees_table();
                    dbDkls.tdcls_id = i+1;
                    dbDkls.tdcls_trakt_id = traktOld.trakt_id;
                    dbDkls.tdcls_plot_id = dkls1.Yta;
                    dbDkls.tdcls_numof = dkls1.Antal;
                    dbDkls.tdcls_spc_id = dkls1.TradslagId;
                    dbDkls.tdcls_spc_name = listaTradslag.Where(x => x.nId == dkls1.TradslagId).FirstOrDefault().sNamn;
                    dbDkls.tdcls_dcls_from = dkls1.Klass;
                    dbDkls.tdcls_dcls_to = dkls1.Klass + diameterIntervall;
                    dbDkls.created = DateTime.Now;
                    dbDkls.tdcls_numof_randtrees = 0;
                    dbDkls.tdcls_hgt = 0.0;
                    dbDkls.tdcls_gcrown = 0.0;
                    dbDkls.tdcls_bark_thick = 0.0;
                    dbDkls.tdcls_m3sk = 0.0;
                    dbDkls.tdcls_m3ub = 0.0;
                    dbDkls.tdcls_grot = 0.0;
                    dbDkls.tdcls_age = 0;
                    dbDkls.tdcls_growth = 0;

                    traktOld.esti_trakt_dcls_trees_table.Add(dbDkls);
                }


                // validera provträd


                // spara ned provträd
                // töm tabellen först
                var deleteOrderDetails2 =
                    from x in traktOld.esti_trakt_sample_trees_table
                    where x.ttree_trakt_id == traktOld.trakt_id
                    select x;

                foreach (var detail in deleteOrderDetails2.Reverse())
                {
                    hmsDB.esti_trakt_sample_trees_table.Remove(detail);
                }

                try
                {
                    hmsDB.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Provide for exceptions.
                }


                Provtrad ptrad = new Provtrad();
                for (int i = 0; i < collection.stmpl.Provtrad.Count(); i++)
                {
                    ptrad = collection.stmpl.Provtrad.ElementAt(i);

                    esti_trakt_sample_trees_table dbPtrad = new esti_trakt_sample_trees_table();
                    dbPtrad.ttree_id = i + 1;
                    dbPtrad.ttree_trakt_id = traktOld.trakt_id;
                    dbPtrad.ttree_plot_id = ptrad.Yta;
                    dbPtrad.ttree_spc_id = ptrad.TradslagId;
                    dbPtrad.ttree_spc_name = listaTradslag.Where(x => x.nId == ptrad.TradslagId).FirstOrDefault().sNamn;
                    dbPtrad.ttree_hgt = ptrad.Hojd;
                    dbPtrad.ttree_dbh = ptrad.Diameter;
                    dbPtrad.ttree_tree_type = 0;    // provträd
                    dbPtrad.ttree_age = ptrad.Alder; // (år)
                    dbPtrad.ttree_gcrown = (ptrad.Gronkronegrans*1.0 / ptrad.Hojd*1.0) * 100.0; // konvertera dm till %
                    dbPtrad.ttree_bark_thick = ptrad.Barktjocklek;   // (mm)
                    dbPtrad.created = DateTime.Now;
                    dbPtrad.ttree_growth = 0;

                    traktOld.esti_trakt_sample_trees_table.Add(dbPtrad);
                }


                // spara ned ändringar från model (trakt) till databasobjekt (traktOld)
                traktOld.trakt_web_diarienr = trakt.trakt_web_diarienr;
                traktOld.trakt_web_bestandsnamn = trakt.trakt_web_bestandsnamn;
                traktOld.trakt_areal = trakt.trakt_areal;
                traktOld.trakt_age = trakt.trakt_age;
                traktOld.trakt_prop_num = trakt.trakt_prop_num;
                traktOld.trakt_prop_name = trakt.trakt_prop_name;
                traktOld.trakt_notes = trakt.trakt_notes;
                traktOld.trakt_si_h100 = trakt.trakt_si_h100;

                hmsDB.Entry(traktOld).State = EntityState.Modified;
                hmsDB.SaveChanges();


                // spara ned inventeringsfiler(na)
                Bestand bestand = new Bestand();
                bestand.InvFiler = new List<InvFil>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;

                    // IE8 bug: parsa ut bara filnamnet
                    string sTmpFilename = Path.GetFileName(hpf.FileName);

                    // ge inventeringsfilen ett unikt filnamn
                    string savedFileName = Path.Combine(Server.MapPath("~/App_Data/uploads"), "(" + Guid.NewGuid() + ") " + sTmpFilename);

                    hpf.SaveAs(savedFileName);
                    bestand.InvFiler.Add(new InvFil(hpf.FileName, savedFileName));
                }

                // Skapa/beräkna rotpost
                int nRet = -1;
                lock (rp)
                {
                    String sCurrentDirectory = "";
                    Directory.SetCurrentDirectory(Server.MapPath("~/bin"));
                    sCurrentDirectory = Directory.GetCurrentDirectory();
                    String sPath = System.Environment.GetEnvironmentVariable("PATH");
                    sCurrentDirectory += (";" + sPath);
                    System.Environment.SetEnvironmentVariable("PATH", sCurrentDirectory);

                    // lägg in nya inventeringsfiler på bestånd och räkna om
                    if (bestand.InvFiler.Count() > 0)
                        nRet = rp.Calculate(bestand.InvFiler, "", User.Identity.Name, trakt.trakt_id);
                    else
                        nRet = rp.Calculate(null, "", User.Identity.Name, trakt.trakt_id);
                }

                // radera inventeringsfiler
                if (bestand.InvFiler.Count() > 0)
                {
                    foreach (InvFil inv in bestand.InvFiler)
                    {
                        System.IO.File.Delete(inv.LagratFilnamn);
                    }
                }

                if (nRet == -1)
                {
                    ViewBag.Error = true;
                    ViewBag.ErrorMessage = rp.GetMessages();
                    return View(collection);
                }


                if (Request.IsAjaxRequest())
                {
                    return Json(new { redirectToUrl = Url.Action("Details", new { id = traktOld.trakt_id }) });
                }
                return RedirectToAction("Details", new { id = traktOld.trakt_id });
            }
            return View(collection);
        }

        //
        // GET: /Bestand/Delete/5

        //[EnhancedAuth("adminGroup")]
        public ActionResult Delete(int id = 0)
        {
            esti_trakt_table trakt = hmsDB.esti_trakt_table.Find(id);
            if (trakt == null)
            {
                return HttpNotFound();
            }
            return View(trakt);
        }

        //
        // POST: /Bestand/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[EnhancedAuth("adminGroup")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                esti_trakt_table trakt = hmsDB.esti_trakt_table.Find(id);
                hmsDB.esti_trakt_table.Remove(trakt);
                hmsDB.SaveChanges();
                return RedirectToAction("Search");
            }
            catch (Exception exp)
            {
                esti_trakt_table trakt = hmsDB.esti_trakt_table.Find(id);
                return View(trakt);
            }
        }


        
        //
        // GET: /Bestand/
        public ActionResult Report1(int id)
        {
            try
            {
                // load report
                ReportDocument reportdocument = new ReportDocument();
                reportdocument.Load(Server.MapPath("~/Content/Reports/HMS_5003.rpt"));

                // apply arguments
                CrystalDecisions.Shared.ParameterDiscreteValue crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                CrystalDecisions.Shared.ParameterValues crParameterValues; // = new CrystalDecisions.Shared.ParameterValues();

                crParameterValues = reportdocument.DataDefinition.ParameterFields["arg1"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = Convert.ToString(id);
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["arg1"].ApplyCurrentValues(crParameterValues);

                crParameterValues = reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = "1";
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].ApplyCurrentValues(crParameterValues);

                // change database settings
                System.Data.EntityClient.EntityConnectionStringBuilder entityConnectionString = new System.Data.EntityClient.EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings["HMS_webEntities"].ConnectionString);
                System.Data.SqlClient.SqlConnectionStringBuilder sConn = new System.Data.SqlClient.SqlConnectionStringBuilder(entityConnectionString.ProviderConnectionString);
                reportdocument.SetDatabaseLogon(sConn.UserID, sConn.Password, sConn.DataSource, sConn.InitialCatalog, true);

                TableLogOnInfo oLogOn;
                foreach (CrystalDecisions.CrystalReports.Engine.Table tbCurrent in reportdocument.Database.Tables)
                {
                    oLogOn = tbCurrent.LogOnInfo;
                    oLogOn.ConnectionInfo.DatabaseName = sConn.InitialCatalog;
                    oLogOn.ConnectionInfo.UserID = sConn.UserID;
                    oLogOn.ConnectionInfo.Password = sConn.Password;
                    oLogOn.ConnectionInfo.ServerName = sConn.DataSource;
                    oLogOn.ConnectionInfo.IntegratedSecurity = sConn.IntegratedSecurity;
                    tbCurrent.ApplyLogOnInfo(oLogOn);
                }


                Stream stream = reportdocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (/*CrystalDecisions.Shared.CrystalReportsException*/ Exception ex)
            {
                TempData["Error"] = true;
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Details", new { id = id });
            }

            return RedirectToAction("Details", new { id = id });
        }



        // Stämplingslängd utökad
        public ActionResult Report2(int id)
        {
            try
            {
                // load report
                ReportDocument reportdocument = new ReportDocument();
                reportdocument.Load(Server.MapPath("~/Content/Reports/HMS_5007.rpt"));

                // apply arguments
                CrystalDecisions.Shared.ParameterDiscreteValue crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                CrystalDecisions.Shared.ParameterValues crParameterValues; // = new CrystalDecisions.Shared.ParameterValues();

                crParameterValues = reportdocument.DataDefinition.ParameterFields["arg1"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = Convert.ToString(id);
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["arg1"].ApplyCurrentValues(crParameterValues);

                crParameterValues = reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = 1;
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].ApplyCurrentValues(crParameterValues);
                
                // change database settings
                System.Data.EntityClient.EntityConnectionStringBuilder entityConnectionString = new System.Data.EntityClient.EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings["HMS_webEntities"].ConnectionString);
                System.Data.SqlClient.SqlConnectionStringBuilder sConn = new System.Data.SqlClient.SqlConnectionStringBuilder(entityConnectionString.ProviderConnectionString);
                reportdocument.SetDatabaseLogon(sConn.UserID, sConn.Password, sConn.DataSource, sConn.InitialCatalog, true);

                TableLogOnInfo oLogOn;
                foreach (CrystalDecisions.CrystalReports.Engine.Table tbCurrent in reportdocument.Database.Tables)
                {
                    oLogOn = tbCurrent.LogOnInfo;
                    oLogOn.ConnectionInfo.DatabaseName = sConn.InitialCatalog;
                    oLogOn.ConnectionInfo.UserID = sConn.UserID;
                    oLogOn.ConnectionInfo.Password = sConn.Password;
                    oLogOn.ConnectionInfo.ServerName = sConn.DataSource;
                    oLogOn.ConnectionInfo.IntegratedSecurity = sConn.IntegratedSecurity;
                    tbCurrent.ApplyLogOnInfo(oLogOn);
                }

                Stream stream = reportdocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (CrystalDecisions.Shared.CrystalReportsException ex)
            {
                TempData["Error"] = true;
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Details", new { id = id });
            }

            return RedirectToAction("Details", new { id = id });
        }


        // Provträdslista
        public ActionResult Report3(int id)
        {
            try
            {
                // load report
                ReportDocument reportdocument = new ReportDocument();
                reportdocument.Load(Server.MapPath("~/Content/Reports/HMS_5006.rpt"));

                // apply arguments
                CrystalDecisions.Shared.ParameterDiscreteValue crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                CrystalDecisions.Shared.ParameterValues crParameterValues; // = new CrystalDecisions.Shared.ParameterValues();

                crParameterValues = reportdocument.DataDefinition.ParameterFields["arg1"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = Convert.ToString(id);
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["arg1"].ApplyCurrentValues(crParameterValues);

                crParameterValues = reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = 1;
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].ApplyCurrentValues(crParameterValues);

               
                // change database settings
                System.Data.EntityClient.EntityConnectionStringBuilder entityConnectionString = new System.Data.EntityClient.EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings["HMS_webEntities"].ConnectionString);
                System.Data.SqlClient.SqlConnectionStringBuilder sConn = new System.Data.SqlClient.SqlConnectionStringBuilder(entityConnectionString.ProviderConnectionString);
                reportdocument.SetDatabaseLogon(sConn.UserID, sConn.Password, sConn.DataSource, sConn.InitialCatalog, true);

                TableLogOnInfo oLogOn;
                foreach (CrystalDecisions.CrystalReports.Engine.Table tbCurrent in reportdocument.Database.Tables)
                {
                    oLogOn = tbCurrent.LogOnInfo;
                    oLogOn.ConnectionInfo.DatabaseName = sConn.InitialCatalog;
                    oLogOn.ConnectionInfo.UserID = sConn.UserID;
                    oLogOn.ConnectionInfo.Password = sConn.Password;
                    oLogOn.ConnectionInfo.ServerName = sConn.DataSource;
                    oLogOn.ConnectionInfo.IntegratedSecurity = sConn.IntegratedSecurity;
                    tbCurrent.ApplyLogOnInfo(oLogOn);
                }

                Stream stream = reportdocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (CrystalDecisions.Shared.CrystalReportsException ex)
            {
                TempData["Error"] = true;
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Details", new { id = id });
            }

            return RedirectToAction("Details", new { id = id });
        }


        //
        // GET: /Bestand/
        public ActionResult Report1p(int id)
        {
            try
            {
                // load report
                ReportDocument reportdocument = new ReportDocument();
                reportdocument.Load(Server.MapPath("~/Content/Reports/HMS_5003p.rpt"));

                // apply arguments
                CrystalDecisions.Shared.ParameterDiscreteValue crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                CrystalDecisions.Shared.ParameterValues crParameterValues; // = new CrystalDecisions.Shared.ParameterValues();

                crParameterValues = reportdocument.DataDefinition.ParameterFields["arg1"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = Convert.ToString(id);
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["arg1"].ApplyCurrentValues(crParameterValues);

                crParameterValues = reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = "1";
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].ApplyCurrentValues(crParameterValues);

                // change database settings
                System.Data.EntityClient.EntityConnectionStringBuilder entityConnectionString = new System.Data.EntityClient.EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings["HMS_webEntities"].ConnectionString);
                System.Data.SqlClient.SqlConnectionStringBuilder sConn = new System.Data.SqlClient.SqlConnectionStringBuilder(entityConnectionString.ProviderConnectionString);
                reportdocument.SetDatabaseLogon(sConn.UserID, sConn.Password, sConn.DataSource, sConn.InitialCatalog, true);

                TableLogOnInfo oLogOn;
                foreach (CrystalDecisions.CrystalReports.Engine.Table tbCurrent in reportdocument.Database.Tables)
                {
                    oLogOn = tbCurrent.LogOnInfo;
                    oLogOn.ConnectionInfo.DatabaseName = sConn.InitialCatalog;
                    oLogOn.ConnectionInfo.UserID = sConn.UserID;
                    oLogOn.ConnectionInfo.Password = sConn.Password;
                    oLogOn.ConnectionInfo.ServerName = sConn.DataSource;
                    oLogOn.ConnectionInfo.IntegratedSecurity = sConn.IntegratedSecurity;
                    tbCurrent.ApplyLogOnInfo(oLogOn);
                }


                Stream stream = reportdocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (/*CrystalDecisions.Shared.CrystalReportsException*/ Exception ex)
            {
                TempData["Error"] = true;
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Details", new { id = id });
            }

            return RedirectToAction("Details", new { id = id });
        }



        // Stämplingslängd utökad
        public ActionResult Report2p(int id)
        {
            try
            {
                // load report
                ReportDocument reportdocument = new ReportDocument();
                reportdocument.Load(Server.MapPath("~/Content/Reports/HMS_5007p.rpt"));

                // apply arguments
                CrystalDecisions.Shared.ParameterDiscreteValue crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                CrystalDecisions.Shared.ParameterValues crParameterValues; // = new CrystalDecisions.Shared.ParameterValues();

                crParameterValues = reportdocument.DataDefinition.ParameterFields["arg1"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = Convert.ToString(id);
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["arg1"].ApplyCurrentValues(crParameterValues);

                crParameterValues = reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].CurrentValues;
                crParameterDiscreteValue = new CrystalDecisions.Shared.ParameterDiscreteValue();
                crParameterDiscreteValue.Value = 1;
                crParameterValues.Add(crParameterDiscreteValue);
                reportdocument.DataDefinition.ParameterFields["Fastighetsinfo"].ApplyCurrentValues(crParameterValues);

                // change database settings
                System.Data.EntityClient.EntityConnectionStringBuilder entityConnectionString = new System.Data.EntityClient.EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings["HMS_webEntities"].ConnectionString);
                System.Data.SqlClient.SqlConnectionStringBuilder sConn = new System.Data.SqlClient.SqlConnectionStringBuilder(entityConnectionString.ProviderConnectionString);
                reportdocument.SetDatabaseLogon(sConn.UserID, sConn.Password, sConn.DataSource, sConn.InitialCatalog, true);

                TableLogOnInfo oLogOn;
                foreach (CrystalDecisions.CrystalReports.Engine.Table tbCurrent in reportdocument.Database.Tables)
                {
                    oLogOn = tbCurrent.LogOnInfo;
                    oLogOn.ConnectionInfo.DatabaseName = sConn.InitialCatalog;
                    oLogOn.ConnectionInfo.UserID = sConn.UserID;
                    oLogOn.ConnectionInfo.Password = sConn.Password;
                    oLogOn.ConnectionInfo.ServerName = sConn.DataSource;
                    oLogOn.ConnectionInfo.IntegratedSecurity = sConn.IntegratedSecurity;
                    tbCurrent.ApplyLogOnInfo(oLogOn);
                }

                Stream stream = reportdocument.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf");
            }
            catch (CrystalDecisions.Shared.CrystalReportsException ex)
            {
                TempData["Error"] = true;
                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Details", new { id = id });
            }

            return RedirectToAction("Details", new { id = id });
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)] 
        public ActionResult AddDiameterklassItem(int? traktId, int? ytaId, int? dklsId)
        {
            DiameterKlass dkls = new DiameterKlass();
            dkls.Antal = 0;
            dkls.Klass = 0;
            dkls.TradslagId = 0;
            dkls.Yta = ytaId;

            var query = hmsDB.esti_trakt_data_table.Where(x => x.tdata_trakt_id == traktId && x.tdata_data_type == 1).OrderBy(x => x.tdata_spc_id);
            foreach (var trslg in query)
            {
                Tradslag t = new Tradslag();
                t.nId = trslg.tdata_spc_id;
                t.sNamn = trslg.tdata_spc_name;

                dkls.Tradslagslista.Add(t);
            }
            
            return PartialView("DiameterklassItem", dkls);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)] 
        public ActionResult AddProvtradItem(int? traktId, int? ytaId, int? dklsId)
        {
            Provtrad ptrad = new Provtrad();
            ptrad.Yta = ytaId;
            ptrad.TradslagId = 0;
            ptrad.Diameter = 0;
            ptrad.Hojd = 0;

            var query = hmsDB.esti_trakt_data_table.Where(x => x.tdata_trakt_id == traktId && x.tdata_data_type == 1).OrderBy(x => x.tdata_spc_id);
            foreach (var trslg in query)
            {
                Tradslag t = new Tradslag();
                t.nId = trslg.tdata_spc_id;
                t.sNamn = trslg.tdata_spc_name;

                ptrad.Tradslagslista.Add(t);
            }

            return PartialView("ProvtradItem", ptrad);
        }
    


    }
}
