﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HMS_web.Models;
using HMS_web.Classes;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace HMS_web.Controllers
{
    public class PaginatedInventerare<T> : PaginatedList<T>
    {
        // filtreringsargument
        public string Namn;
        public string EfterNamn;
        public string SkapadAv;
        public string OrderBy;
        public int? OrderByDesc;

        public PaginatedInventerare(IQueryable<T> source, int pageIndex, int pageSize)
            : base(source, pageIndex, pageSize)
        {
        }
    }

    // [Authorize(Roles = "Administrator")]   // Commented out only to make testing faster/easier
    public class InventerareController : Controller
    {
        private HMS_webEntities db = new HMS_webEntities();

        //
        // GET: /Inventerare/

        public ActionResult Index()
        {
            return View(db.web_inventerare.ToList());
        }

        //
        // GET: /Inventerare/Details/5

        public ActionResult Details(int id = 0)
        {
            web_inventerare web_inventerare = db.web_inventerare.Find(id);
            if (web_inventerare == null)
            {
                return HttpNotFound();
            }
            return View(web_inventerare);
        }

        //
        // GET: /Inventerare/Create
        public ActionResult Create()
        {
            // Don't allow this method to be called directly.
/*            if (this.HttpContext.Request.IsAjaxRequest() != true)
                return RedirectToAction("Index", "Inventerare"); */
            
//            return View();
            var model = new web_inventerare();
            return View("Create", model);
        }

        //
        // POST: /Inventerare/Create
        [HttpPost]
        public ActionResult Create(web_inventerare web_inventerare)
        {
            if (ModelState.IsValid)
            {
                web_inventerare.created = DateTime.Now;
                db.web_inventerare.Add(web_inventerare);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(web_inventerare);
        }

        //
        // GET: /Inventerare/Save
        [OutputCache(Duration = 0)]
        public ActionResult Save()
        {
            // Don't allow this method to be called directly.
            if (this.HttpContext.Request.IsAjaxRequest() != true)
                return RedirectToAction("Index", "Inventerare"); 

            var model = new Inventerare();
            return PartialView("_CreateInventerare", model);
        }

        //
        // POST: /Inventerare/Save
        [HttpPost]
        public JsonResult Save(web_inventerare data)
        {
            if (data.inventerare_namn != null && data.inventerare_efternamn != null)
            {
                // check if combination of names exists in db
                var query = db.web_inventerare.Where(a => (a.inventerare_efternamn == data.inventerare_efternamn &&
                    a.inventerare_namn == data.inventerare_namn)).FirstOrDefault();

                if (query == null)  // record does not exist, save it
                {
                    if (ModelState.IsValid)
                    {
                        /*web_inventerare inv = new web_inventerare();
                        inv.inventerare_namn = data.inventerare_namn;
                        inv.inventerare_efternamn = data.inventerare_efternamn;
                        inv.created = data;

                        db.web_inventerare.Add(inv);*/
                        db.web_inventerare.Add(data);
                        db.SaveChanges();

                        // get last id
                        /*inv = db.web_inventerare.OrderByDescending(a => a.inventerare_id).First();
                        data.Id = inv.inventerare_id;*/

                        return Json(data);
                    }
                }
            }

            data.inventerare_id = -1;
            return Json(data);
        }


        //
        // GET: /Inventerare/Edit/5

        [EnhancedAuth("adminGroup")]
        public ActionResult Edit(int id = 0)
        {
            web_inventerare web_inventerare = db.web_inventerare.Find(id);
            if (web_inventerare == null)
            {
                return HttpNotFound();
            }
            return View(web_inventerare);
        }

        //
        // POST: /Inventerare/Edit/5

        [HttpPost]
        [EnhancedAuth("adminGroup")]
        public ActionResult Edit(web_inventerare web_inventerare)
        {
            if (ModelState.IsValid)
            {
                db.Entry(web_inventerare).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(web_inventerare);
        }

        //
        // GET: /Inventerare/Delete/5

        [EnhancedAuth("adminGroup")]
        public ActionResult Delete(int id = 0)
        {
            web_inventerare web_inventerare = db.web_inventerare.Find(id);
            if (web_inventerare == null)
            {
                return HttpNotFound();
            }
            return View(web_inventerare);
        }

        //
        // POST: /Inventerare/Delete/5

        [HttpPost, ActionName("Delete")]
        [EnhancedAuth("adminGroup")]
        public ActionResult DeleteConfirmed(int id)
        {
            web_inventerare web_inventerare = db.web_inventerare.Find(id);
            db.web_inventerare.Remove(web_inventerare);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }







        // GET: /Inventerare/CreateAjax
        [OutputCache(Duration = 0)]
        public ActionResult CreateAjax()
        {
            if (this.HttpContext.Request.IsAjaxRequest() == true)
            {
                var model = new web_inventerare();    //new Inventerare();
                return PartialView("CreateAjax", model);
            }

            return View();
        }

        //
        // POST: /Inventerare/CreateAjax

        [HttpPost]
        public ActionResult CreateAjax(web_inventerare inventerare)  //(Inventerare inventerare)
        {
            var query = db.web_inventerare.Where(a => (a.inventerare_namn == inventerare.inventerare_namn &&
                a.inventerare_efternamn == inventerare.inventerare_efternamn)).FirstOrDefault();

            if (query == null)  // record does not exist, save it
            {

                if (ModelState.IsValid)
                {
                    try {
                        inventerare.created = DateTime.Now;
                        inventerare.created_by = User.Identity.Name;
                        db.web_inventerare.Add(inventerare);
                        db.SaveChanges();

                        // save went alright
                        return Json(inventerare);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }
                }
            }

            // something is not entered correctly
            inventerare.inventerare_id = -1;
            return Json(inventerare);
        }

        //
        // GET: /Inventerare/ChooseAjax

        public ActionResult ChooseAjax(int? page, string orderBy, int? orderByDesc, string namn, string efterNamn, string reset = "")
        {
            // Don't allow this method to be called directly.
            if (this.HttpContext.Request.IsAjaxRequest() != true)
                return RedirectToAction("Index", "Fastighet");

            const int pageSize = 10;

            if (reset == null || reset == "")
            {
                if (namn == null) namn = "";
                if (efterNamn == null) efterNamn = "";
            }
            else
            {
                ModelState.Clear();

                namn = "";
                efterNamn = "";
                page = 0;
                orderBy = "";
            }


            IQueryable<web_inventerare> dbInv;
            dbInv = from inv in db.web_inventerare
                    //join propowner in db.fst_prop_owner_table on fast.id equals propowner.prop_id
                    //join owner in db.fst_contacts_table on propowner.contact_id equals owner.id
                    //TODO: join i linq kräver att elementen finns i bägge tabellerna! testa med group join into.
                    where
                    (namn == "" || inv.inventerare_namn.Contains(namn)) &&
                    (efterNamn == "" || inv.inventerare_efternamn.Contains(efterNamn))
                    select inv;
            /*if (User.IsInRole(System.Configuration.ConfigurationManager.AppSettings["adminGroup"])) // are we member of the admin group?
            {
                dbInv = from inv in db.web_inventerare
                        //join propowner in db.fst_prop_owner_table on fast.id equals propowner.prop_id
                        //join owner in db.fst_contacts_table on propowner.contact_id equals owner.id
                        //TODO: join i linq kräver att elementen finns i bägge tabellerna! testa med group join into.
                        where
                        (namn == "" || inv.inventerare_namn.Contains(namn)) &&
                        (efterNamn == "" || inv.inventerare_efternamn.Contains(efterNamn))
                        select inv;
            }
            else
            {
                dbInv = from inv in db.web_inventerare
                        //join propowner in db.fst_prop_owner_table on fast.id equals propowner.prop_id
                        //join owner in db.fst_contacts_table on propowner.contact_id equals owner.id
                        //TODO: join i linq kräver att elementen finns i bägge tabellerna! testa med group join into.
                        where
                        (namn == "" || inv.inventerare_namn.Contains(namn)) &&
                        (efterNamn == "" || inv.inventerare_efternamn.Contains(efterNamn)) &&
                        (inv.created_by == User.Identity.Name)
                        select inv;
            }*/

            switch (orderBy)
            {
                case "inventerare_namn":
                    if (orderByDesc == 1) dbInv = dbInv.OrderBy(a => a.inventerare_namn);
                    else dbInv = dbInv.OrderByDescending(a => a.inventerare_namn);
                    break;
                default:
                    if (orderByDesc == 1) dbInv = dbInv.OrderBy(a => a.inventerare_efternamn);
                    else dbInv = dbInv.OrderByDescending(a => a.inventerare_efternamn);
                    break;
            }

            var paginatedInv = new PaginatedInventerare<web_inventerare>(dbInv, page ?? 0, pageSize);

            paginatedInv.Namn = namn;
            paginatedInv.EfterNamn = efterNamn;
            paginatedInv.OrderBy = orderBy;
            paginatedInv.OrderByDesc = orderByDesc;

            return PartialView(paginatedInv);
        }
   
    
    
    
    
    
    
    
    }
}