﻿using HMS_web.Classes;
using HMS_web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMS_web.Controllers
{
    public class BestandsmallController : Controller
    {
        HMS_webEntities hmsDB = new HMS_webEntities();

        //
        // GET: /Bestandsmall/

        public ActionResult Index()
        {
            var query = hmsDB.tmpl_template_table.Where(x => x.tmpl_type_of != -1).ToList();
            if (query != null)
            {
                //return View(query);
                List<Bestandsmall> bestandsmallar = new List<Bestandsmall>();
                foreach (var template in query)
                {
                    string szTemplateXml = template.tmpl_template;
                    Bestandsmall bm = new Bestandsmall();
                    bm.ParseXml(szTemplateXml);

                    bestandsmallar.Add(bm);
                }

                return View(bestandsmallar);
            }

            return View();
        }

        //
        // GET: /Bestandsmall/Details/5

        public ActionResult Details(int id)
        {
            // Don't allow this method to be called directly.
            if (this.HttpContext.Request.IsAjaxRequest() != true)
                return RedirectToAction("Index", "Bestandsmall");

            var query = hmsDB.tmpl_template_table.Where(a => a.tmpl_id == id).Select(a => a.tmpl_template);
            if (query != null)
            {
                string szTemplateXml = query.First();
                Bestandsmall bm = new Bestandsmall();
                bm.ParseXml(szTemplateXml);
                return PartialView(bm);
            }

            return RedirectToAction("Index");
        }

        //
        // GET: /Bestandsmall/Create

/*        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Bestandsmall/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Bestandsmall/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Bestandsmall/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Bestandsmall/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Bestandsmall/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }*/
    }
}
