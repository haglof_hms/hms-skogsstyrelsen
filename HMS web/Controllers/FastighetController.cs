﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HMS_web.Models;
using HMS_web.Classes;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace HMS_web.Controllers
{
    public class PaginatedFastighet<T> : PaginatedList<T>
    {
        // filtreringsargument
        public string FastNamn;
        public /*DateTime?*/ string DatumFran;
        public /*DateTime?*/ string DatumTill;
        public string FastNummer;
        public string Agare;
        public string Inventerare;
        public string SkapadAv;
        public string OrderBy;
        public int? OrderByDesc;

        public PaginatedFastighet(IQueryable<T> source, int pageIndex, int pageSize)
            : base(source, pageIndex, pageSize)
        {
        }
    }
    
    public class FastighetController : Controller
    {
        private HMS_webEntities db = new HMS_webEntities();

        //
        // GET: /Fastighet/

        public ActionResult Index(int? Page)
        {
            const int pageSize = 10;
            string Namn = "";

            var dbFastighet = db.fst_property_table.Where(a => a.prop_name.Contains(Namn)).OrderBy(a => a.id);
            var paginatedTrakt = new PaginatedList<fst_property_table>(dbFastighet, Page ?? 0, pageSize);
            return View(paginatedTrakt);
        }

        //
        // GET: /Fastighet/Details/5

        public ActionResult Details(int id = 0)
        {
            fst_property_table fst_property_table = db.fst_property_table.Find(id);
            if (fst_property_table == null)
            {
                return HttpNotFound();
            }
            return View(fst_property_table);
        }

        //
        // GET: /Fastighet/Create

        public ActionResult Create()
        {
            return View();
        }


        //
        // POST: /Fastighet/Create

        [HttpPost]
        public ActionResult Create(fst_property_table fst_property_table)
        {
            if (ModelState.IsValid)
            {
                db.fst_property_table.Add(fst_property_table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fst_property_table);
        }

        //
        // GET: /Fastighet/Edit/5

        [EnhancedAuth("adminGroup")]
        public ActionResult Edit(int id = 0)
        {
            fst_property_table fst_property_table = db.fst_property_table.Find(id);
            if (fst_property_table == null)
            {
                return HttpNotFound();
            }
            return View(fst_property_table);
        }

        //
        // POST: /Fastighet/Edit/5

        [HttpPost]
        [EnhancedAuth("adminGroup")]
        public ActionResult Edit(fst_property_table fst_property_table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fst_property_table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fst_property_table);
        }

        //
        // GET: /Fastighet/Delete/5

        [EnhancedAuth("adminGroup")]
        public ActionResult Delete(int id = 0)
        {
            fst_property_table fst_property_table = db.fst_property_table.Find(id);
            if (fst_property_table == null)
            {
                return HttpNotFound();
            }
            return View(fst_property_table);
        }

        //
        // POST: /Fastighet/Delete/5

        [HttpPost, ActionName("Delete")]
        [EnhancedAuth("adminGroup")]
        public ActionResult DeleteConfirmed(int id)
        {
            fst_property_table fst_property_table = db.fst_property_table.Find(id);
            db.fst_property_table.Remove(fst_property_table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        //
        // GET: /Fastighet/ChooseAjax

        public ActionResult ChooseAjax(int? page, string orderBy, int? orderByDesc, string fastNamn, string fastNummer, DateTime? datumFran, DateTime? datumTill, string reset = "")
        {
            // Don't allow this method to be called directly.
            //if (this.HttpContext.Request.IsAjaxRequest() != true)
                //return RedirectToAction("Index", "Fastighet");

            const int pageSize = 10;

            if (reset == null || reset == "")
            {
                if (fastNamn == null) fastNamn = "";
                if (fastNummer == null) fastNummer = "";
                if (orderBy == null) orderBy = "";
            }
            else
            {
                ModelState.Clear();

                fastNamn = "";
                fastNummer = "";
                page = 0;
                datumFran = null;
                datumTill = null;
                orderBy = "";
            }


            IQueryable<fst_property_table> dbFastighet;
            dbFastighet = from fast in db.fst_property_table
                          //join propowner in db.fst_prop_owner_table on fast.id equals propowner.prop_id
                          //join owner in db.fst_contacts_table on propowner.contact_id equals owner.id
                          //TODO: join i linq kräver att elementen finns i bägge tabellerna! testa med group join into.
                        where
                        (fastNamn == "" || fast.prop_name.Contains(fastNamn)) &&
                        (fastNummer == "" || fast.prop_number.Contains(fastNummer)) &&
                        (datumFran == null || fast.created > datumFran) &&
                        (datumTill == null || fast.created < datumTill)
                        select fast;

            switch (orderBy)
            {
                case "trakt_prop_name":
                    if (orderByDesc == 1) dbFastighet = dbFastighet.OrderBy(a => a.prop_name);
                    else dbFastighet = dbFastighet.OrderByDescending(a => a.prop_name);
                    break;
                case "trakt_created_by":
                    if (orderByDesc == 1) dbFastighet = dbFastighet.OrderBy(a => a.prop_name);
                    else dbFastighet = dbFastighet.OrderByDescending(a => a.prop_name);
                    break;
                default:
                    if (orderByDesc == 1) dbFastighet = dbFastighet.OrderBy(a => a.prop_name);
                    else dbFastighet = dbFastighet.OrderByDescending(a => a.prop_name);
                    break;
            } 

            //var dbFastighet = db.fst_property_table.Where(a => a.prop_name.Contains(namn)).OrderBy(a => a.id);
            var paginatedFast = new PaginatedFastighet<fst_property_table>(dbFastighet, page ?? 0, pageSize);

            paginatedFast.FastNummer = fastNummer;
            paginatedFast.FastNamn = fastNamn;
            paginatedFast.DatumFran = datumFran.ToString();
            paginatedFast.DatumTill = datumTill.ToString();
            paginatedFast.OrderBy = orderBy;
            paginatedFast.OrderByDesc = orderByDesc;

            return PartialView(paginatedFast);
        }


        //
        // GET: /Fastighet/CreateAjax
        public ActionResult CreateAjax()
        {
            if (this.HttpContext.Request.IsAjaxRequest() == true)
            {
                var model = new fst_property_table();   // Fastighet();
                return PartialView("CreateAjax", model);
            }

            return View();
        }

        //
        // POST: /Fastighet/CreateAjax

        [HttpPost]
        public ActionResult CreateAjax(fst_property_table fastighet)
        {
            if (ModelState.IsValid)
            {
                // Kontrollera att inte fastighetsnummer redan finns
                var query = db.fst_property_table.Where(a => a.prop_number == fastighet.prop_number).FirstOrDefault();

                if (query != null)  // 
                {
                    ModelState.AddModelError("prop_number", "Fastighetsnummer finns redan!");
                    fastighet.id = -1; 
                    return Json(fastighet);
                }

                try {
                    //fastighet.created = DateTime.Now;
                    fastighet.created_by = User.Identity.Name;
                    fastighet.created = DateTime.Now;
                    fastighet.prop_full_name = fastighet.prop_name + " " + fastighet.block_number + ":" + fastighet.unit_number;
                    db.fst_property_table.Add(fastighet);
                    db.SaveChanges();

                    //fastighet = db.fst_property_table.OrderByDescending(a => a.id).First();

                    // save went alright
                    return Json(fastighet);
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }

            // something is not entered correctly
            fastighet.id = -1;
            return Json(fastighet);
        }

    }
}