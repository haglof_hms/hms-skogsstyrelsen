﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HMS_web.Models;
using HMS_web.Classes;

namespace HMS_web.Controllers
{
    public class KontaktController : Controller
    {
        private HMS_webEntities db = new HMS_webEntities();

        //
        // GET: /Kontakt/

        /*public ActionResult Index()
        {
            return View(db.fst_contacts_table.ToList());
        }*/
        public ActionResult Index(int? Page)
        {
            const int pageSize = 10;
            string Namn = "";

            var dbKontakt = db.fst_contacts_table.Where(a => a.name_of.Contains(Namn)).OrderBy(a => a.id);
            var paginatedKontakt = new PaginatedList<fst_contacts_table>(dbKontakt, Page ?? 0, pageSize);
            return View(paginatedKontakt);
        }

        //
        // GET: /Kontakt/Details/5

        public ActionResult Details(int id = 0)
        {
            fst_contacts_table fst_contacts_table = db.fst_contacts_table.Find(id);
            if (fst_contacts_table == null)
            {
                return HttpNotFound();
            }
            return View(fst_contacts_table);
        }

        //
        // GET: /Kontakt/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Kontakt/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(fst_contacts_table fst_contacts_table)
        {
            if (ModelState.IsValid)
            {
                db.fst_contacts_table.Add(fst_contacts_table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fst_contacts_table);
        }

        //
        // GET: /Kontakt/Edit/5

        [EnhancedAuth("adminGroup")]
        public ActionResult Edit(int id = 0)
        {
            fst_contacts_table fst_contacts_table = db.fst_contacts_table.Find(id);
            if (fst_contacts_table == null)
            {
                return HttpNotFound();
            }
            return View(fst_contacts_table);
        }

        //
        // POST: /Kontakt/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [EnhancedAuth("adminGroup")]
        public ActionResult Edit(fst_contacts_table fst_contacts_table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fst_contacts_table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fst_contacts_table);
        }

        //
        // GET: /Kontakt/Delete/5

        [EnhancedAuth("adminGroup")]
        public ActionResult Delete(int id = 0)
        {
            fst_contacts_table fst_contacts_table = db.fst_contacts_table.Find(id);
            if (fst_contacts_table == null)
            {
                return HttpNotFound();
            }
            return View(fst_contacts_table);
        }

        //
        // POST: /Kontakt/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [EnhancedAuth("adminGroup")]
        public ActionResult DeleteConfirmed(int id)
        {
            fst_contacts_table fst_contacts_table = db.fst_contacts_table.Find(id);
            db.fst_contacts_table.Remove(fst_contacts_table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}